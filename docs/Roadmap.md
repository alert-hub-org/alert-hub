CAPCollator Roadmap - Last updated II 16th Nov 2024

```mermaid
gantt
    title Alert-Hub.org CAP Collator Roadmap
    dateFormat  YYYY-MM-DD
    excludes    weekends

    section Release 3 rollout
    implement multi-bucket pub       :t3-0, 2024-12-01, 5d
    Test S3 Publication              :t3-1, after t3-0, 5d
    Release 3                        :milestone, after t3-1, 0d
    Review Rollout Plan              :t3-1a, after t3-1, 5d
    Provision alternate S3           :t3-2, after t3-1a, 2d
    Verify alternate S3              :t3-3, after t3-2, 1d
    Decomission v2                   :t3-4, after t3-3, 1d
    Enable v3 publishing             :t3-5, after t3-4, 1d

    section Micronaut Migration
    Task Scheduler Library           :t4-1, 2025-01-01, 10d
```
