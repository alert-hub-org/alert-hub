package alerthub

import grails.plugin.springsecurity.annotation.Secured;
import feedfacade.SourceFeed;

class HomeController {

  def springSecurityService
  def feedCheckerService

  @Secured('IS_AUTHENTICATED_ANONYMOUSLY')
  def index() { 
    log.debug("HomeController::index");
    def user = springSecurityService.currentUser

    def result = [:]

    // if ( user ) {
    //   redirect(controller:'home', action:'dash');
    // }
    // else {
      result.feeds=SourceFeed.executeQuery('select sf from SourceFeed as sf order by sf.enabled desc, sf.uriname');
    // }

    result
  }

  def log() {
    def result = [:]
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def dash() {
    log.debug("HomeController::home");
    def result = [:]
    redirect controller:'sourcefeed', action:'index'
    result
  }



  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def login() {
    log.debug("HomeController::login");
    redirect action:'index'
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def profile() {
    log.debug("HomeController::profile");
    def result = [:]
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def changePassword() {

    def user = springSecurityService.currentUser

    log.debug("HomeController::changePassword ${params}");
    if ( ( params.newpass?.length() > 0 ) && ( params.newpass == params.confirm ) ) {
      user.password = params.newpass
      if ( user.save(flush:true, failOnError:true) ) {
        flash.message = "Password Updated!"
      }
    }

    def result = [:]
    redirect action:'profile'
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def doLogout() {
    log.debug("HomeController::logout");
    request.logout()
    redirect action:'index'
  }

  def status() {
    log.debug("HomeController::status");
    def result = [:]
    result.enabledSummary = SourceFeed.executeQuery('select count(*),sf.enabled from SourceFeed as sf group by sf.enabled').collect { [ count:it[0], status:it[1] ] }
    result.checkerStatusSummary = SourceFeed.executeQuery('select count(*),sf.status from SourceFeed as sf group by sf.status').collect { [ count:it[0], status:it[1] ] }
    result.feedStatusSummary = SourceFeed.executeQuery('select count(*),sf.capAlertFeedStatus from SourceFeed as sf group by sf.capAlertFeedStatus').collect { [ count:it[0], status:it[1] ] }
    result.activeChecks = feedCheckerService.getActiveTaskReport()
    result.lastLog = feedCheckerService.getLastLog()

    result
  }
}
