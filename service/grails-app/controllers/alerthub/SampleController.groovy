package alerthub

import alerthub.*;
import grails.converters.JSON;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;
import java.nio.file.Files;
import java.nio.file.Path;
import grails.rest.RestfulController
import grails.converters.XML
import java.nio.charset.Charset
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper

class SampleController {

  def ESWrapperService

  def index() {
		log.info("SampleController::index ${params}");

    // Create a byte array output stream to hold the zip data
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()
    ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)

		String[] feeds = params.feeds?.split(',');
		int sample_size = params.count ? Integer.parseInt(params.count) : 100;
		String method = params.method ?: 'latest';
		String additional = params.additional ?: null;

    try {
      // Add each file in the directory to the zip output stream
			feeds?.each { feedstr ->
				String feed = feedstr.trim();
				sampleToStream(feed, sample_size, method, zipOutputStream, additional);
			}

			zipOutputStream.flush();
      zipOutputStream.close()
      byteArrayOutputStream.close()

      // Set the response headers
      response.contentType = 'application/zip'
      response.setHeader('Content-Disposition', 'attachment; filename="alerthub-sample.zip"')
      response.outputStream << byteArrayOutputStream.toByteArray() // Write the zip file to response
      response.outputStream.flush()
    } catch (Exception e) {
      // Handle errors
      log.error "Error creating zip file: ${e.message}"
      render status: 500, text: "Error creating zip file."
    }
	}

	private void sampleToStream(String feed, int max, String method, ZipOutputStream zipOutputStream, String qry) {

		log.info("sampleToStream(\"${feed}\",${max},${method},...,${qry})");

    // Default to live index
    String index_to_search = grailsApplication.getConfig().getProperty('indexes.AlertsArchive.name');

    if ( params.context=='archive' )
      index_to_search = grailsApplication.getConfig().getProperty('indexes.AlertsArchive.name');

    List<String> query_clauses = []
    query_clauses.add('{ "prefix": { "AlertMetadata.sourceFeed.keyword":"'+feed+'"} }');
    // query_clauses.add('{ "term": { "AlertMetadata.sourceFeed.keyword":"'+feed+'"} }');

		if ( qry != null )
			query_clauses.add('{ "query_string": { "query": "'+qry+'" } }');

    String es_query = null;

		if ( method=='random' ) {
			es_query = '''{ 
        "query": { 
					"function_score": {
						"query": {
              "bool": { 
                "must": [ '''+query_clauses.join(', ')+''' ] 
							}
						},
						"functions": [ { "random_score": { } } ]
					}
				}
			}'''
		}
		else {
			es_query = '''{ "query": { "bool": { "must": [ '''+query_clauses.join(',\n')+''' ] } } }'''
		}

    // String es_query = '{ "query": { "term": { "AlertMetadata.sourceFeed.keyword":"'+feed+'"} } }';
		log.info("Sample es query: ${es_query}");


		int offset = 0;
    try {
      var qr = ESWrapperService.searchJson(index_to_search, es_query, offset, max, 'AlertBody.sent.keyword', 'desc');

      if ( qr != null ) {
        long totalAlerts = qr.totalHits
				log.info("Processing ${totalAlerts} alerts for sample of ${feed}");

		    zipOutputStream.putNextEntry(new ZipEntry("${feed}.info"));
				zipOutputStream << "${es_query}:${totalAlerts}".getBytes();
				zipOutputStream.closeEntry()

        List<Map<String,Object>> rows = convertHitsToObjects(qr.hits)
				rows.each { alert ->
					Map<String,Object> alertMetadata = alert.get('AlertMetadata');
					String snowflake_id = alertMetadata.get('snowflakeId');
					String alert_xml = alertMetadata.get('cached_alert_xml');
		      zipOutputStream.putNextEntry(new ZipEntry("${feed}-${snowflake_id}.xml"));
			    zipOutputStream << alert_xml.getBytes();
				  zipOutputStream.closeEntry()
				}
      }
			else {
				log.info("No records found for ${feed}");
			}
    }
    catch ( Exception e ) {
      log.error("Problem running ES Query to find alerts for sub query ${e.message}",e);
    }
	}

  private List<Map<String,Object>> convertHitsToObjects(List<co.elastic.clients.elasticsearch.core.search.Hit> hits) {

    List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
    ObjectMapper mapper = new ObjectMapper();
  
    if ( hits != null ) {
      hits.each { hit ->
        com.fasterxml.jackson.databind.node.ObjectNode hit_obj = hit.source();
        Map<String, Object> hit_as_map = mapper.convertValue(hit_obj, new TypeReference<Map<String, Object>>(){});
        result.add(hit_as_map)
      }
    }

    return result;
  } 

}

