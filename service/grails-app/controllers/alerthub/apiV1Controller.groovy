package alerthub

import alerthub.dto.PreflightCommand;
import alerthub.dto.AlertNotificationDetail;
import alerthub.dto.NotifyResult;
import alerthub.dto.AlertNotificationCommand
import capcollator.CapUrlHandlerService
import capcollator.CapEventHandlerService
import feedfacade.SourceFeed
import grails.converters.JSON;
import grails.plugin.springsecurity.annotation.Secured;

import alerthub.utils.UUIDUtils



class apiV1Controller {

	CapUrlHandlerService capUrlHandlerService;
	CapEventHandlerService capEventHandlerService
	private static java.lang.String CAP_NAMESPACE = 'CAP';

	/* notify this node of a new alert.
	 *
	 * @see: https://grails.github.io/grails-spring-security-rest/latest/docs/
	 * Example curl command
	 * curl -H "Content-Type: application/json"
	 *      -H "accept: application/json"
	 *      -X POST
	 *      -d '{"alerts":[ { url:"https://some.url", source:"al-source-1" } ], "sourceSystem":"ah", "signature":"aaa"}'
	 *      http://localhost:8080/CAPAggregator/api/v1/notifyAlert
	 */
	@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
	def notifyAlert(AlertNotificationCommand notification) {

		long starttime=System.currentTimeMillis();

		log.info("notifyAlert, queue depth is ${capEventHandlerService.getQueueDepth()}");
		try {
			if ( capEventHandlerService.getQueueDepth() > 20 ) {
				log.warn("Queue depth > 20, returning 503 retry in 10");
				Thread.sleep(1000);
				response.setHeader('Retry-After', '10') // Optional: Suggest retry after 10s
				render status: 503, text: 'Service is temporarily unavailable. Please try again later.'
			}
			else {
				log.info("apiV1Controller::notifyAlert(...)");
				Map<String,Object> context = [:];
				for ( AlertNotificationDetail alert: notification.getAlerts() ) {
					if ( alert.getSource() != null ) {
						context.'feed-code' = alert.getSource();
					}

					if ( notification.getForceProcessing() != null )
						context.'forceProcessing' = notification.getForceProcessing();
					else
						context.'forceProcessing' = Boolean.FALSE;

					log.debug("handle ${alert}")
					capUrlHandlerService.handleNotification(alert.getUrl(),context);
				}
	
				NotifyResult notify_result = NotifyResult.builder()
						  .resultCode("OK")
							.build();

				log.info("notifyAlert complete");
				render notify_result as JSON;
			}
		}
		catch ( Exception e ) {
			log.error("Problem in notifyAlert",e);
			throw(e);
		}
		finally {
			log.info("notifyAlert completed in ${System.currentTimeMillis() - starttime}");
		}
	}

	@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
  def preflight(PreflightCommand preflightCommand) {
    Map result = [
      unknownIdentifiers:[]
    ]

    preflightCommand.identifiers?.each { id ->
			String uuid = generateAlertUUIDString(id);
			result.unknownIdentifiers.add(['id':id, 'uuid':uuid]);
    }   

    render result as JSON;
  }     



  private static String generateAlertUUIDString(String cap_identifier) {
    String alert_uuid_str = null;
    try {
      // log.info("generate uuid for ${cap_identifier}");
      UUID CAP_UUID = UUIDUtils.dnsUUID(CAP_NAMESPACE);
      UUID alert_uuid = UUIDUtils.nameUUIDFromNamespaceAndString(CAP_UUID, cap_identifier );
      alert_uuid_str = alert_uuid.toString();
      // log.debug("ALERT-UUID: ${cap_identifier} == ${alert_uuid_str}")
    }
    catch ( Exception e ) {
      // log.error("Problem in UUID genertion, e");
      throw(e);
    }
    return alert_uuid_str;
  }

}
