package alerthub

import grails.plugin.springsecurity.annotation.Secured
import static grails.async.Promises.*
import feedfacade.SourceFeed;

class AdminController {

  def newEventService
  def feedCheckerService
  def systemService
  def sourceListService
  def CAPIndexingService
  def subsImportService


  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def index() { 
    log.debug("AdminController::index");
    def result = [:]
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def notificationLog() {
    log.debug("AdminController::notificationLog");
    def result = [:]
    result.log = newEventService.eventLog
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def feedCheckerLog() {
    [feedCheckerLog:feedCheckerService.getLastLog()]
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def enableAll() {
    log.debug("Call systemService.enableAllOperating()");
    SourceFeed.withTransaction {
      systemService.enableAllOperating();
    }
    redirect(url: request.getHeader('referer'))
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def disableAll() {
    systemService.disableAll();
    redirect(url: request.getHeader('referer'))
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def status() {
    def result=[:]
    result.activeTasks=feedCheckerService.getActiveTaskReport()
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def refreshPublicFeeds() {
    def result=[:]
    SourceFeed.withTransaction {
      sourceListService.checkStaticConfig(grailsApplication.config.fah.staticConfigUrl);
    }
    redirect(url: request.getHeader('referer'))
  }

  @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
  def registerConsumer() {
  }

  @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
  def reindex() {
    log.debug("AdminController::Reindex");
    CAPIndexingService.reindexSubscriptions()
    redirect(controller:'home',action:'index');
  }

  @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
  def syncSubList() {
    if (params.subUrl) {
      String target_url = params.subUrl
      log.debug("Attempting to parse list of subs from \"${target_url}\"");

      def import_promise = task {
        subsImportService.loadSubscriptionsFrom(target_url)
      }

      import_promise.onComplete {
        log.debug("subsImportService.loadSubscriptionsFrom - completed");
      }

      import_promise.onError { Throwable err ->
        println "An error occured ${err.message}"
      }

      log.debug("Started background loader task");
    }

    return [status:subsImportService.getStatus()];

  }

}
