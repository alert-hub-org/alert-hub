package alerthub

class UrlMappings {

    static mappings = {

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:'home', action:'index')
        "/topics/$id"(controller:'home', action:'topic')
        "/status"(controller:'home', action:'status')
        "/api/v1/$action"(controller:'apiV1')

        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
