package feedfacade

import grails.plugin.springsecurity.annotation.Secured



class SubscriptionController {

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def index() { 
    log.debug("SourcefeedController::index");
    def result = [:]
    result.subscriptions=WebhookSubscription.executeQuery('select s from Subscription as s');
    result
  }

  @Secured(['ROLE_USER', 'IS_AUTHENTICATED_FULLY'])
  def newRabbitQueue() {
    log.debug("SourcefeedController::newRabbitQueue");
    def result = [:]

    WebhookSubscription.withTransaction {
      // Create a new subscription to a topic which fires a RabbitMQ message to a named queue
      def s = new WebhookSubscription(
                               guid:java.util.UUID.randomUUID().toString(),
                               callback:params.queueName,
                               topic:Topic.findByName(params.topicName),
                               subType:'pubsub'
                              ).save(flush:true, failOnError:true);
      log.debug("RabbitMQ Subscription created...");
    }
    redirect(action:'index');
  }

}
