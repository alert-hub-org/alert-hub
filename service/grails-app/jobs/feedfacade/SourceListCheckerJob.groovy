package feedfacade

class SourceListCheckerJob {
  
  def sourceListService

  static triggers = {
    // Cron:: Min Hour DayOfMonth Month DayOfWeek Year
    // At 0s, 5m past 2am on Sunday
    cron name:'dailyJob', startDelay:180000, cronExpression: "0 5 2 * * ?"
    // cronExpression: "s m h D M W Y"
    //                  | | | | | | `- Year [optional]
    //                  | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
    //                  | | | | `- Month, 1-12 or JAN-DEC
    //                  | | | `- Day of Month, 1-31, ?
    //                  | | `- Hour, 0-23
    //                  | `- Minute, 0-59
    //                  `- Second, 0-59
  }

  def execute() {
    log.debug("Synchronize with static config");
    // sourceListService.setUpSources(grailsApplication.config.fah.sourceList);
    if ( grailsApplication.config.fah.staticConfigUrl instanceof String ) {
      sourceListService.checkStaticConfig(grailsApplication.config.fah.staticConfigUrl)
    }
  }
}
