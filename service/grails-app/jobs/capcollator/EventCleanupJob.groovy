package capcollator


import grails.core.GrailsApplication
import java.text.SimpleDateFormat
import capcollator.ESWrapperService;

class EventCleanupJob {

  ESWrapperService ESWrapperService
  GrailsApplication grailsApplication

  static triggers = {
    simple name: 'ESCleanupJobTrigger', repeatInterval: 300000l, startDelay:30000l // execute job once in 300 seconds
  }

  def execute() {

    // Current timestamp
    SimpleDateFormat ts_sdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS'Z'".toString());
    ts_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
    def now = ts_sdf.format(new Date())

    String live_alerts_index = grailsApplication.getConfig().getProperty('indexes.AlertsLive.name');


    log.debug("EventCleanupJob expunging alerts where expire less than or equal ${now} in index ${live_alerts_index}");
    
    try {
      // ESWrapperService.deleteByJsonQuery(live_alerts_index,'{ "range" : { "AlertMetadata.Expires" : { "lte" : "'+now+'" } } }');
      ESWrapperService.deleteByJsonQuery(live_alerts_index,"AlertMetadata.Expires:[* TO ${now}]".toString());

      // long one_hour_ago_ms = System.currentTimeMillis() - ( 1 * 3600 * 1000 ) // 1 hour, 3600 seconds in an hour, 1000ms in a second
      // String one_hour_ago = ts_sdf.format(new Date(one_hour_ago_ms))
      // ESWrapperService.deleteByJsonQuery('events','{ "range" : { "timestamp" : { "lte" : "'+one_hour_ago+'" } } }');
    }
    catch ( Exception e ) {
      log.warn("Problem in EventCleanupJob - ${e.message}");
    }
    finally {
      log.info("Event cleanup completed");
    }
  }
}
