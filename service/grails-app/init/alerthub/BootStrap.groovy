package alerthub

import alerthub.LocalFeedSettings
import capcollator.CAPIndexingService
import feedfacade.SourceFeed
import feedfacade.SourceListService
import grails.plugin.springsecurity.web.filter.GrailsAnonymousAuthenticationFilter;
import grails.util.Environment
import feedfacade.FlagDefinition;

import grails.core.GrailsApplication
import org.springframework.beans.factory.annotation.Value

class BootStrap {

  CAPIndexingService CAPIndexingService;
  GrailsApplication grailsApplication;
  // def servletContext
  AlertHubSystemService alertHubSystemService;
  SourceListService sourceListService;

  @Value('${info.app.version}')
  String explicitAppVersion

  def init = { servletContext ->

    String localFeedSettingsFilename = grailsApplication.config.getProperty('fah.localFeedSettings')
    String staticConfigUrl = grailsApplication.config.getProperty('fah.staticConfigUrl','https://gitlab.com/alert-hub-org/alert-hub/-/raw/main/service/config/sample_static_config.json?ref_type=heads');

    log.info("AlertHub Starting.....");
    log.info("  -> metadata version : ${grailsApplication.metadata.getApplicationVersion()}");
    log.info("  -> explicitAppVersion : ${explicitAppVersion}");
    log.info("  -> version : ${grailsApplication.config.getProperty('info.app.version')}");
    log.info("  -> datasource.url : ${grailsApplication.config.getProperty('dataSource.url')}");
    log.info("  -> datasource.username : ${grailsApplication.config.getProperty('dataSource.username')}");
    log.info("  -> datasource.dialect : ${grailsApplication.config.getProperty('dataSource.dialect')}");
    log.info("  -> datasource.driverClassName : ${grailsApplication.config.getProperty('dataSource.driverClassName')}");
    log.info("  -> grails.serverUrl : ${grailsApplication.config.getProperty('grails.serverUrl')}");
    log.info("  -> localFeedSettingsFilename : ${localFeedSettingsFilename}");
    log.info("  -> eshost : ${grailsApplication.config.getProperty('esuser')}");
    log.info("  -> eshost : ${grailsApplication.config.getProperty('eshost')}");

    log.debug("Validate that required ES indexes are present");
    CAPIndexingService.validate()

    
    LocalFeedSettings.withTransaction { status ->
      // load local overrrides first
      if ( localFeedSettingsFilename != null ) {
				log.debug("Checking for local feed settings file: ${localFeedSettingsFilename}");
        File local_feed_settings_file = new File(localFeedSettingsFilename)
        if ( local_feed_settings_file.canRead() ) {
          log.debug("Attempting to read local feed settings from ${grailsApplication.config.fah.localFeedSettings}");
          alertHubSystemService.loadLocalFeedSettings("file://${local_feed_settings_file}");
          sourceListService.loadLocalFeedSettings("file://${local_feed_settings_file}");
        }
        else {
          log.warn("Unable to locate local feed settings file: ${localFeedSettingsFilename}");
        }
      }
      else {
        if ( staticConfigUrl != null ) {
          log.debug("Call sourceListService.checkStaticConfig");
          sourceListService.checkStaticConfig(staticConfigUrl);
        }
      }
    } 

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        log.info("alerthub shutdown hook - process terminating");
      }
    });


    User.withTransaction { status ->
      // Only set up default accounts in the test and development environments
      if ( ( Environment.currentEnvironment.name == Environment.DEVELOPMENT ) ||
           ( Environment.currentEnvironment.name == Environment.TEST ) ) {
        log.debug("Setting up dev and test user accounts");
        setUpUserAccounts();
      }
      else {
        log.info("Production configuration");
      }

      // Ensure we have all flags defined (TTLs are given in milliseconds)
      [
        [ code:'InvalidFeedPubDate',       name:'Invalid Feed Pub Date',      type:'Warn', ttl:60*60*24*7*1000,
          advice: 'Check the format of the date element at the root of your feed. Dates should be formatted according to RFC 3339 for ATOM and RFC 822 for RSS' ],
        [ code:'InvalidItemPubDate',       name:'Invalid Item Pub Date',      type:'Warn', ttl:60*60*24*7*1000,
          advice: 'Check the format of date elements for items in your feed. Dates should be formatted according to RFC 3339 for ATOM and RFC 822 for RSS' ],
        [ code:'UnexpectedContentType',    name:'Unexpected Content Type',    type:'Warn', ttl:60*60*24*7*1000,
          advice: 'Your web server should return a content type of application/xml, application/rss+xml, application/atom+xml or text/xml' ],
      ].each { flag_definition ->
        FlagDefinition.findByCode(flag_definition.code) ?: new FlagDefinition(flag_definition).save(flush:true, failOnError:true);
      }
    }

    alertHubSystemService.init()

    SourceFeed.withTransaction {
			log.info("Clearing any blocked feeds at startup");
			long MAX_TIME_FOR_PROCESSING = 120000; // 2Mins
      SourceFeed.executeUpdate('update SourceFeed sf set sf.status=:paused where sf.status=:inProcess and sf.lastStarted < :mtpAgo',
                                 [inProcess:'in-process', paused:'paused', mtpAgo:System.currentTimeMillis()-MAX_TIME_FOR_PROCESSING])
    }


  }

  def setUpUserAccounts() {

    List users_to_configure = grailsApplication.getConfig().getProperty('sysusers', List, new ArrayList<Map>());

    log.debug("Configure users: ${users_to_configure}");
    users_to_configure.each { su ->
      log.debug("user name:${su.name} ${su.pass} display-as:${su.display} roles:${su.roles}");
      def user = User.findByUsername(su.name)
      if ( user ) {
        if ( ( user.password == null ) && ( su.pass == null ) ) {

          log.debug("Hard change of user password from config ${user.password} -> ${su.pass}");
          user.password = su.pass;
          user.save(failOnError: true)
        }
        else {
          log.debug("${su.name} present and correct");
        }
      }
      else {
        String password = null;
        if ( su.pass == null ) {
          password = grailsApplication.config.defaultAdmPassword ?: java.util.UUID.randomUUID().toString()
          log.info("Generated password for ${su.name} -> ${password}");
          println("\n\n*** ${password} ***\n\n");
        }
        else {
          password = su.pass;
        }

        log.debug("Create user...${su.name} ");

        user = new User(
                      username: su.name,
                      password: password,
                      display: su.display,
                      email: su.email,
                      enabled: true).save(failOnError: true)
      }

      log.debug("Add roles for ${su.name} (${su.roles})");
      su.roles.each { r ->

        def role = Role.findByAuthority(r) ?: new Role(authority:r).save(flush:true, failOnError:true)

        if ( ! ( user.authorities.contains(role) ) ) {
          log.debug("  -> adding role ${role} (${r})");
          UserRole.create user, role
        }
        else {
          log.debug("  -> ${role} already present");
        }
      }
    }

    SourceFeed.executeUpdate('update SourceFeed set status=:paused where capAlertFeedStatus=:op or capAlertFeedStatus=:test',[paused:'paused',op:'operating',test:'testing']);

  }


  def destroy = {
  }
}
