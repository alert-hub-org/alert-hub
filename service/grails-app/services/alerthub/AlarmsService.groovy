package alerthub;

public class AlarmsService {

	public void raiseAlarm(
		String alarmURI,
		String resource,
		String alarmCode) {
		log.debug("Raising alarm: ${alarmURI} / ${resource} / ${alarmCode}");
	}
}
