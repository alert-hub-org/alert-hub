package feedfacade

import grails.gorm.transactions.*
import java.security.MessageDigest

import java.util.Calendar
import java.util.TimeZone

import org.hibernate.Session
import org.hibernate.StatelessSession
import alerthub.LocalFeedSettings;

@Transactional
class SourceListService {

  /**
   * Allow a FeedFacade to be bootstrapped from locally specified content. The URL must point to a json file structured as
   * {
   *    alertRegisters:[
   *      'https://s3-eu-west-1.amazonaws.com/alert-hub-sources/json'
   *    ]
   * }
   */
  public void checkStaticConfig(String static_config_url) {
    log.info("SourceListService::checkStaticConfig(static_config_url: ${static_config_url})");
    if ( static_config_url ) {
			log.debug("Processing static conofig url");
      try {
        def live_json_data = new groovy.json.JsonSlurper().parse(new java.net.URL(static_config_url))

				log.debug("Got static config");

        if ( live_json_data ) {
          live_json_data.alertRegisters.each { alert_source_register ->
						log.debug("Setting up source register from static config ${alert_source_register}");
            setUpSources(alert_source_register);
          }
					if ( live_json_data.overrides != null ) {
  	  	  	ingestLocalFeedSettings(live_json_data.overrides);
					}
					else {
						log.info("No overrides present");
					}
        }
				else {
					log.warn("Unable to parse static config json");
				}
      }
      catch ( Exception e ) {
        log.error("Problem parsing static config", e);
      }
    }
		else {
			log.warn("Bypass");
		}
    
  }

  public void setUpSources(String source_url) {
    try {
      log.info("SourceListService::setUpSources(source_url: ${source_url})");

      // We add an enabled flag which defaults to false. Sources must now be added then enabled in the web interface.
      SourceFeed.executeUpdate('update SourceFeed set enabled = :enabled where enabled is null',[enabled:false]);

      Map<String,Object> live_json_data = new groovy.json.JsonSlurper().parse(new java.net.URL(source_url))

      ingestCapFeeds(live_json_data.get('sources'));
    }
    catch ( Exception e ) {
      log.error("problem syncing cap feed list",e);
    }

  }

  public void loadLocalFeedSettings(String source_url) {
    try {
      Map<String, Object> live_json_data = new groovy.json.JsonSlurper().parse(new java.net.URL(source_url))
      ingestLocalFeedSettings(live_json_data)
    }
    catch ( Exception e ) {
      log.error("problem syncing cap feed list",e);
    }
  }

  private void ingestCapFeeds(List<Map<String,Object>> fd) {
    log.info("SourceListService::ingestCapFeeds - ${fd.size()} entries");
    Map<String, Topic> topic_cache = new HashMap<String, Topic>();
    fd?.each { s ->
      long start_timestamp = System.currentTimeMillis();
      try {
        // Array of maps containing a source elenment
        String capAlertFeedStatus = s.source?.capAlertFeedStatus?.toLowerCase() ?: 'unknown';
        if ( ( s.source != null ) && (!capAlertFeedStatus.equals('bypassed')) )  {
          log.debug("Validate source id=${s.source.sourceId}");

          SourceFeed.withNewSession { session ->
            SourceFeed.withTransaction { status ->
              SourceFeed source = SourceFeed.findByUriname(s.source.sourceId)
              if ( source == null ) {
                log.debug("  --> Create (enabled:false)");
                source = new SourceFeed(
                                         uriname: s.source.sourceId,
                                         name: s.source.sourceName,
                                         status:'paused',
                                         baseUrl:s.source.capAlertFeed,
                                         lastCompleted:Long.valueOf(0),
                                         processingStartTime:Long.valueOf(0),
                                         capAlertFeedStatus: capAlertFeedStatus,
                                         pollInterval:60*1000,
                                         enabled:false,
                                         feedBrandingUrl: s.source.feedBrandingUrl).save(flush:true, failOnError:true);

                log.debug("adding tags");
                source.addTag('sourceIsOfficial',"${s.source.sourceIsOfficial}");
                source.addTag('sourceLanguage',"${s.source.sourceLanguage}");
                source.addTag('authorityCountry',"${s.source.authorityCountry}");
                source.addTag('authorityAbbrev',"${s.source.authorityAbbrev}");
                source.addTag('registerUrl',"${s.source.registerUrl}");
                source.addTag('logoUrl',"${s.source.logoUrl}");
                source.addTag('author',"${s.source.author}");
                source.addTag('guid',"${s.source.guid}");
                log.debug("Adding topics");
                source.addTopics("${s.source.sourceId},AllFeeds,${s.source.authorityCountry},${s.source.authorityAbbrev}", topic_cache)
              }
              else {
                if ( ( ! source.baseUrl.equals(s.source.capAlertFeed) ) ||
                     ( ! (source.capAlertFeedStatus?:'').equals(s.source.capAlertFeedStatus?:'') )  ||
                     ( ! (source.name?:'').equals(s.source.sourceName?:'') ) ||
                     ( ! (source.feedBrandingUrl?:'').equals(s.source.feedBrandingUrl?:'') ) ) {
                  log.debug("  --> changed :: (db)url:${source.baseUrl}/name:${source.name}/status:${source.capAlertFeedStatus}  != (new)${url:s.source.capAlertFeed}/name:${s.source.sourceName}/status:${s.source.capAlertFeedStatus}. Update.. (enabled:${source.enabled})");
                  source.baseUrl = s.source.capAlertFeed;
                  source.name = s.source.sourceName;
                  source.capAlertFeedStatus = s.source.capAlertFeedStatus?.toLowerCase();
                  source.feedBrandingUrl = s.source.feedBrandingUrl;
                  source.save(flush:true, failOnError:true);
                  log.info("Updated: base url: ${source.baseUrl} / ${source.capAlertFeedStatus}");
                }
                else {
                  log.debug("  --> unchanged (enabled:${source.enabled})");
                }
              }
            }
            session.clear();
          }
        }
      }
      catch ( Exception e ) {
        log.error("Problem trying to add or update entry ${s.source}",e);
      }
      long elapsed = System.currentTimeMillis() - start_timestamp;
      log.info("upsert feed: ${elapsed}ms");

    }
    log.info("Complete");
  }

  private void ingestLocalFeedSettings(Map<String,Object> fd) {
		log.debug("** Looking for local feed setting overrides");
    fd?.each { k, v ->
      log.debug("Processing ${k}");
      try {
        // Array of maps containing a source elenment
        if ( k ) {
          log.debug("Validate source ${k}");
          def source = LocalFeedSettings.findByUriname(k)
          if ( source == null ) {
            source = new LocalFeedSettings(
                                           uriname: k,
                                           alternateFeedURL: v.alternateFeedURL,
                                           authenticationMethod: v.authenticationMethod,
                                           credentials: v.credentials).save(flush:true, failOnError:true);
          }
          else {
            source.alternateFeedURL = v.alternateFeedURL
            source.authenticationMethod = v.authenticationMethod
            source.credentials = v.credentials
            source.save(flush:true, failOnError:true);
          }
        }
      }
      catch ( Exception e ) {
        log.error("Problem trying to add or update entry ${s?.uriname}",e);
      }
    }
  }

}

