package feedfacade

import grails.gorm.transactions.*
import java.security.MessageDigest
import alerthub.services.pubsub.AlertHubEventPublisher;
import alerthub.services.pubsub.AlertHubEvent;
import alerthub.utils.Utils;

import groovy.xml.XmlUtil
import groovy.util.Node;
import groovy.util.XmlNodePrinter;

@Transactional
class NewEventService {

  
  def recent_notifications = new org.apache.commons.collections4.queue.CircularFifoQueue(100);
  AlertHubEventPublisher alertHubEventPublisher;
  def grailsApplication
  
  def handleNewEvent(Long feed_id, Map<String, Object> entryInfo) {

    log.info("NewEventService::handleNewEvent(${feed_id}) type=${entryInfo.type}");

    Entry.withNewTransaction {


      def entry_title = entryInfo.title
      def entry_summary = entryInfo.summary
      def entry_description = entryInfo.description
      def entry_link = entryInfo.link
      def entry_type = entryInfo.type
      def feed_copyright = entryInfo.feedCopyright ?: 'unspecified';

  	  log.debug("Feed copyright: ${feed_copyright}");
      if ( entry_title?.length() > 255 ) {
        // log.debug("Trim title...");
        entry_title = entry_title.substring(0,254);
      }

      log.debug("type:${entry_type} title:\"${entry_title}\" summary:\"${entry_summary}\" desc:\"${entry_description}\" link:\"${entry_link}\"");

      def entry = domNodeToString(entryInfo.sourceDoc)

      def entryHash = hashEntry(entry);

      // log.debug("Make sure that we don't already have an entry for feed/hash ${feed_id} ${entryHash}");

      if ( entry?.length() > 0 ) {
  
        def existingEntries = Entry.executeQuery('select e.id from Entry as e where e.ownerFeed.id = :owner_id and e.entryHash = :hash',[owner_id:feed_id, hash:entryHash])
  
        if ( existingEntries.size() == 0 ) {
          // log.debug("None found -- create");
  
          Entry e = null
          def uriname = null;
  
          Entry.withNewTransaction {
            log.debug("New Entry:: ${feed_id} ${entryHash}");
            def owner_feed = SourceFeed.get(feed_id)
            uriname = owner_feed.uriname
            String json_text = Utils.XmlToJson(entry);
  
            e = new Entry ( 
                              ownerFeed: owner_feed,
                                  title: entry_title,
                            description: entry_summary,
                                   link: entry_link,
                              entryHash: entryHash,
                                  entry: entry,
                            entryAsJson: json_text,
                                entryTs: System.currentTimeMillis()).save(flush:true, failOnError:true);
          }
  
          publish(entry_type, feed_id, uriname, e, feed_copyright)
        }
        else {
          log.warn("Entry is a repeated hash (feed ${feed_id}, hash:${entryHash})");
        }
      }
      else {
        log.error("Result of domNodeToString was null");
      }
    }
  }

  def hashEntry(entry) {
    MessageDigest md5_digest = MessageDigest.getInstance("MD5");
    md5_digest.update(entry.getBytes())
    byte[] md5sum = md5_digest.digest();
    new BigInteger(1, md5sum).toString(16);
  }

  public String domNodeToString(groovy.util.Node node) {
    //Create stand-alone XML for the entry
    String result = null;
    try {
      // result =  groovy.xml.XmlUtil.serialize(node)
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      XmlNodePrinter nodePrinter = new XmlNodePrinter(pw);
      nodePrinter.setPreserveWhitespace(true);
      nodePrinter.print(node);
      result = sw.toString();
    }
    catch ( Exception e ) {
      log.error("Input was ${node?.class.name} ${node}");
      log.error("Problem in domNodeToString",e);
      throw e;
    }
    finally {
      log.debug("domNodeToString complete");
    }
    return result;
  }

  public void publish(String entry_type, Long feed_id, String feed_code, Entry entry, String feed_copyright) {

    log.debug("NewEventService::publish(${entry_type},${feed_id},${feed_code},...) - ${entry.getLink()}");

    // Here is where we may publish to RabbitMQ.
    publishInternalEvent("CAPExchange", entry_type, feed_id, feed_code, entry, feed_copyright);

    // Publish down our traditional route.
    publishToSubscriptions(feed_id, entry);
  }

  Object publishInternalEvent(String target_exchange, String entry_type, Long feed_id, String feed_code, Entry entry, String feed_copyright) {

    log.debug("NewEventService::publishInternalEvent(${target_exchange},${entry_type},${feed_id},...) - ${entry.getLink()}");

    Object result = null;

    try {

      log.debug("Publishing event with code alerthub.${entry_type}");

      alertHubEventPublisher.publish(
        AlertHubEvent.builder()
          .eventCode("alerthub."+entry_type)
          .exchange(target_exchange)
          .routingKey(entry_type+'.'+entry.ownerFeed.uriname)
          .headers([
                'feed-id':feed_id,
                'feed-code':feed_code,
                'entry-id':entry.id,
                'feed-url':entry.ownerFeed.baseUrl,
                'feed-is-official':entry.ownerFeed.isTagged('sourceIsOfficial','true'),
                'feed-copyright':feed_copyright
              ])
          .body([
            'entryAsJson':entry.getEntryAsJson(),
            'entry': entry
          ])
          .build());

      log.debug("Completed call to alertHubEventPublisher.publish for alerthub.${entry_type} with routing key ${entry_type}.${entry.ownerFeed.uriname} / ${entry.getLink()}");
    }
    catch ( Exception e ) {
      log.error("Problem trying to publish to pubsub",e);
    }

    return result
  }

  def publishToSubscriptions(Long feed_id, Entry entry) {

    log.debug("publish to subscriptions : ${feed_id}, ${entry.getLink()}")


    // Find all subscriptions where the sub has a topic which intersects with any of the topics for this feed
    def subscriptions = WebhookSubscription.executeQuery('select s from WebhookSubscription as s where exists ( select ft from FeedTopic as ft where ft.topic = s.topic and ft.ownerFeed.id = :id )',[id:feed_id]);

    // Iterate through all subscriptions
    subscriptions.each { sub ->

      if ( sub?.trimNs?.equalsIgnoreCase('y') ) {
        // log.debug("Trim namespaces, regardless of JSON or XML response");
      }
      else {
      }

      // log.debug("Got xml_text... target mime type is ${sub.targetMimetype}");

      def result = null;

      switch ( sub.targetMimetype ) {
        case 'json':
          // See snippet here https://gist.github.com/ianibo/fe36ab6220f820b1cd49
          result = entry.entryAsJson
          break;

        default:
          // log.debug("Notify via XML");
          result=entry.entry
          break;
      }

      WebhookSubscriptionEntry se = new WebhookSubscriptionEntry(
                                                   owner:sub, 
                                                   entry:entry, 
                                                   eventDate:new Date(), 
                                                   status:'pending', 
                                                   reason:"Event published on feed ${feed_id} with matching topic").save(flush:true, failOnError:true);

      // log.debug("done");
    }
  }

  def getEventLog() {
    return recent_notifications
  }
}
