package capcollator;

import alerthub.Alert
import alerthub.AlertHubSystemService
import alerthub.LocalFeedSettings
import alerthub.services.pubsub.AlertHubEvent
import alerthub.services.pubsub.AlertHubEventPublisher
import alerthub.utils.AlertWrapper
import alerthub.utils.Snowflake
import alerthub.utils.UUIDUtils
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import groovyx.net.http.ApacheHttpBuilder
import groovyx.net.http.ChainedHttpConfig
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import org.apache.commons.collections4.map.PassiveExpiringMap
import org.apache.commons.io.input.BOMInputStream
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.HttpClientBuilder
import alerthub.utils.DuplicateAlertException

/**
 * This is where all CAP URLs detected in feeds, be they atom, RSS, or other source types come to be resolved.
 */
@Transactional
class CapUrlHandlerService {

  private static int MAX_HTTP_TIME = 8*1000; // 4s
  private static byte[] stylesheet_pattern = '<?xml-stylesheet '.getBytes();
  private static java.lang.String CAP_NAMESPACE = 'CAP';


  AlertHubEventPublisher alertHubEventPublisher;
  
  StaticFeedService staticFeedService
  FeedFeedbackService feedFeedbackService
  Snowflake snowflake;
  GrailsApplication grailsApplication;
  AlertHubSystemService alertHubSystemService;

  // Alerts can live in the cache for up to 2 minutes
  private Map parsed_alert_cache = Collections.synchronizedMap(new PassiveExpiringMap(1000*60*2))
  private Map local_feed_setting_cache = [:]

  private static final long LONG_ALERT_THRESHOLD = 2000;
  private static final int MAX_RETRIES = 3;

  def process(cap_url) {
    log.debug("CapUrlHandlerService::process ${cap_url}");
  }

  def getLocalFeedSettings(String feed_code) {
    def result = local_feed_setting_cache[feed_code];
    if ( result == null ) {
      LocalFeedSettings.withNewTransaction {
        LocalFeedSettings lfs = LocalFeedSettings.findByUriname(feed_code)
        if ( lfs != null ) {
          result = [ status: 'ModPresent', authenticationMethod: lfs.authenticationMethod, credentials:lfs.credentials ]
        }
        else {
          result = [ status: 'NoModification' ]
        }
        local_feed_setting_cache[feed_code] = result;
      }
    }
    return result;
  }

  def handleNotification(String link, Map<String,Object> context) {

    log.info("CapUrlHandlerService::handleNotification(${link},${context}) - svchash=${this.hashCode()}");

    def ts_1 = System.currentTimeMillis();
    def cap_link = link.toString()

    String source_feed = context['feed-code'];
    String source_id = context['feed-id'];
    String is_official = context['feed-is-official'];
    String feed_copyright = context['feed-copyright'];

		if ( ( feed_copyright != null ) && ( feed_copyright.equals('unspecified' ) ) ) {
			feed_copyright = '';
		}
    String original_cap_link = cap_link

    def lfs = getLocalFeedSettings(source_feed);
    if ( lfs?.status=='ModPresent' ) {
      switch ( lfs.authenticationMethod ) {
        case 'pin':
          cap_link += "?pin=${lfs.credentials}"
          break;
        default:
          break;
      }

      log.debug("modifed URL: ${cap_link}");
    }


    log.debug("Looking in link attribute for cap ${link}");

    // Different feeds behave differently wrt properly setting the type attribute. 
    // Until we get to grips a little better - try and parse every link - and if we manage to parse XML, see if the root node is a cap element

    log.info("  -> processing link ${link} via ${source_feed}");

    boolean completed_ok = false;
    int retries = 0;
    
    while ( !completed_ok && retries < MAX_RETRIES ) {
      try {
        def ts_2 = System.currentTimeMillis();

        log.debug("test ${cap_link}");
        def detected_content_type = null

        HttpBuilder http_client = ApacheHttpBuilder.configure {
          request.uri = cap_link
          client.clientCustomizer { HttpClientBuilder builder ->
            RequestConfig.Builder requestBuilder = RequestConfig.custom()
            requestBuilder.connectTimeout = MAX_HTTP_TIME
            requestBuilder.connectionRequestTimeout = MAX_HTTP_TIME
            builder.defaultRequestConfig = requestBuilder.build()
          }
        }

        String response_content = http_client.get {

          request.headers['Accept'] = 'application/cap+xml,application/xml,text/xml,*.*'

          response.parser('application/xml') { ChainedHttpConfig cfg, FromServer fs ->
            fs.inputStream.text
          }
          response.parser('application/octet-stream') { ChainedHttpConfig cfg, FromServer fs ->
            fs.inputStream.text
          }
          response.parser('text/xml') { ChainedHttpConfig cfg, FromServer fs ->
            fs.inputStream.text
          }
          response.parser('application/cap+xml') { ChainedHttpConfig cfg, FromServer fs ->
            fs.inputStream.text
          }

          response.failure { FromServer resp ->
            log.warn("Failure fetching content : ${resp?.getContentType()} ${resp?.getStatusCode()} ${resp?.getMessage()} ${resp}")
            return null;
          }

          response.success { resp, content ->
            detected_content_type = FromServer.Header.find( resp.headers, 'Content-Type')?.value
            return content;
          }

        }

        log.info("URL Connection reports content type (In response to HEAD) ${detected_content_type}");

        if ( ( detected_content_type != null ) &&
             ( detected_content_type.toLowerCase().startsWith('text/xml') ||
               detected_content_type.toLowerCase().startsWith('application/octet-stream') ||   // Because of http://www.gestiondelriesgo.gov.co
               detected_content_type.toLowerCase().startsWith('application/cap+xml') ||
               detected_content_type.toLowerCase().startsWith('application/xml') ) ) {

          def parser = new XmlSlurper()

          def fetch_completed = System.currentTimeMillis();

          byte[] alert_bytes = response_content.getBytes()


          // It would be nice to see if we can extract any stylesheet referenced as <?xml-stylesheet href='capatomproduct.xsl' type='text/xsl'?>
          // from the first n bytes
          String xml_stylesheet = findStylesheet(alert_bytes, cap_link)

          // In preparation for dropping XMLSlurper we create a variable capable of holding the jackson databinding
          // version of an alert parsed into a POJO
          alerthub.cap.Alert pojoCap = null;
          try {
            JacksonXmlModule xmlModule = new JacksonXmlModule();
            xmlModule.setDefaultUseWrapper(false);
            XmlMapper xmlMapper = new XmlMapper(xmlModule);
            // Read the XML file
            pojoCap = xmlMapper.readValue(alert_bytes, alerthub.cap.Alert.class);

            log.debug("POJO Alert: ${pojoCap}");
          }
          catch ( Exception e ) {
            log.error("problem in JACKSON xml parsing of CAP from ${cap_link}: ",e.toString())
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            alertHubSystemService.logSystemEvent([
                    'alarm':'JacksonParseError',
                    'resource': cap_link,
                    'message': e.toString()
                    // 'stacktrace': sw.toString()
            ])
						return;
          }

					log.info("Proceed to process");

          parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false) 
          parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

          def bais = new ByteArrayInputStream(alert_bytes);
					def bom_is = new BOMInputStream(bais)
					if (bom_is.hasBOM() == false) {
						log.info("No BOM in input stream ");
					} 
					else {
						log.warn("BOM detected in input stream");
					} 

          def parsed_cap = parser.parse(bom_is);

          String alert_uuid_str = generateAlertUUIDString(parsed_cap.identifier.toString() );
          if ( alert_uuid_str == null )
            throw new RuntimeException("NULL ALERT UUID GENERATED");

					List processingFlags = [];

          if ( ( context['forceProcessing'] != null ) && ( context['forceProcessing'] == Boolean.TRUE ) ) {
            log.info("Force processing on on ${alert_uuid_str} / ${parsed_cap.identifier?.toString()}")
						processingFlags.add("FORCE");
					}
					else if ( existsInArchive(alert_uuid_str) ) {
            log.info("DUPLICATE_ALERT_ID: ${alert_uuid_str} already present in archive for \"${parsed_cap.identifier?.toString()}\" - bailing");
            throw new DuplicateAlertException("DUPLICATE ALERT ID ${alert_uuid_str} (exit after ${System.currentTimeMillis() - ts_2}ms)".toString());
          }
          else {
            log.info("already exists check on ${alert_uuid_str} reports false for ${parsed_cap.identifier?.toString()}")
          }

          String alert_snowflake_id = snowflake.nextId();

          if ( ( parsed_cap != null ) && ( parsed_cap.identifier != null ) ) {

            String cached_alert_xml = null;
            // We only write the alert XML if it's actually a cap alert that we managed to parse.
            if ( grailsApplication.getConfig().getProperty('featureFilesystemCache', Boolean.class, Boolean.TRUE) == Boolean.TRUE) {
              // If the fielsystem feature is enabled write the alert to the filesystem
              cached_alert_xml = staticFeedService.writeAlertXML(alert_bytes, source_feed, new Date(ts_2), alert_snowflake_id)
            }
            else {
              // otherwise don't
              cached_alert_xml = new String(alert_bytes);
            }


            def ts_3 = System.currentTimeMillis();
            log.info("Managed to parse link ${cap_link} as ${detected_content_type} from ${source_feed}, looks like CAP :: handleNotification ::\"${parsed_cap.identifier}\"");

            def entry = domNodeToString(parsed_cap)

            def latest_expiry = null;
            def latest_effective = null;
            parsed_cap.info.each { info_element ->
              latest_expiry = info_element.expires?.text()
              latest_effective = info_element.effective?.text()
            }

            log.debug("latest_expiry is ${latest_expiry}");
            def alert_metadata = [:]
            alert_metadata.createdAt=System.currentTimeMillis()
            alert_metadata.CCHistory=[]

            alert_metadata.CCHistory.add(["event":"CC-RSS-notified","timestamp":ts_1]);
            alert_metadata.CCHistory.add(["event":"CC-HTTP Get completed","timestamp":fetch_completed]);
            alert_metadata.CCHistory.add(["event":"CC-parse complete","timestamp":ts_3]);
            alert_metadata.CCHistory.add(["event":"CC-emit CAP event","timestamp":alert_metadata.createdAt]);

            def elapsed = alert_metadata.createdAt - ts_1;
            if ( elapsed > LONG_ALERT_THRESHOLD ) {
              log.info("Alert processing exceeded LONG_ALERT_THRESHOLD(${elapsed}) ${cap_link}");
            }

            alert_metadata.detectedStylesheetPI = xml_stylesheet
            alert_metadata.hasStylesheet = (xml_stylesheet == null) ? 'N' : 'Y'
            alert_metadata.PrivateSourceUrl = cap_link
            alert_metadata.SourceUrl = original_cap_link
            alert_metadata.capCollatorUUID = alert_uuid_str;
            alert_metadata.snowflakeId = alert_snowflake_id;
            alert_metadata.sourceFeed = source_feed;
            alert_metadata.sourceIsOfficial = is_official;
            alert_metadata.cached_alert_xml = cached_alert_xml;
            alert_metadata.feed_copyright = feed_copyright;

            if ( latest_expiry && latest_expiry.trim().length() > 0 )
              alert_metadata.Expires = latest_expiry

            if ( latest_effective && latest_effective.trim().length() > 0 )
              alert_metadata.Effective = latest_effective

            Map<String, Object> body=alerthub.utils.Utils.XmlToMap(entry,false);

						List<String> alarms = validateCap(pojoCap);

            AlertWrapper aw = AlertWrapper.builder()
            .rawCAP(alert_bytes)
            .pojoCAP(pojoCap)
            .alertBody(body)
            .alertMetadata(alert_metadata)
            .url(link)
						.alarms(alarms)
						.processingFlags(processingFlags)
            .build();

            // http://www.nws.noaa.gov/geodata/ tells us how to understand geocode elements
            // Look at the various info.area elements - if the "polygon" element is null see if we can find an info.area.geocode we understand well enough to expand
            broadcastCapEvent(aw, source_feed)

            completed_ok = true;
          }
          else {
            log.warn("No valid CAP from ${cap_link} -- consider improving rules for handling this");
            feedFeedbackService.publishFeedEvent(source_feed,
                                                 source_id,
                                                 "No valid CAP found at ${cap_link}");
            completed_ok = true;
          }
        }
        else {
          log.warn("${cap_link} (content type ${detected_content_type}) - unable to parse");
          completed_ok = true;
        }
      }
      catch ( DuplicateAlertException dae ) {
        log.info("Bypass duplicate alert");
        completed_ok=true;
      }
			catch ( java.lang.IllegalArgumentException iae ) {
        log.error("Connection timeout: ${cap_link} ${context} ${ste.message}");
        feedFeedbackService.publishFeedEvent(source_feed,
                                             source_id,
                                             "IllegalArgumentException processing CAP URL elapsed ${System.currentTimeMillis()-ts_1}): ${cap_link} ${iae.message}");
        completed_ok=true;
			}
      catch ( java.net.SocketTimeoutException ste ) {
        log.error("Connection timeout: ${cap_link} ${context} ${ste.message}");
        // sleep for .5s before retry
        Thread.sleep(500)

        feedFeedbackService.publishFeedEvent(source_feed,
                                             source_id,
                                             "TIMEOUT processing CAP URL (retry ${retries}/elapsed ${System.currentTimeMillis()-ts_1}): ${cap_link} ${ste.message}");

        retries++;
      }
      catch ( Exception e ) {
        log.error("problem handling cap alert ${cap_link} ${context} ${e.message}", e);
        // sleep for .5s before retry
        Thread.sleep(500)

        feedFeedbackService.publishFeedEvent(source_feed,
                                             source_id,
                                             "problem processing CAP URL (retry ${retries}/elapsed ${System.currentTimeMillis()-ts_1}): ${cap_link} ${e.message}");


        retries++;
      }
      finally {
        log.info("CAP URL Checker Task Complete for ${link} via ${source_feed}");
      }
    }
  }

  private boolean existsInArchive(String uuid) {
    return Alert.exists(uuid);
  }

  private String findStylesheet(byte[] alert_bytes, String cap_link) {
    String stylesheet=null;
    int start_of_pi =  indexOf(alert_bytes, stylesheet_pattern, 350);
    try {
      if ( start_of_pi >= 0 ) {
        int start_of_stylesheet = start_of_pi+16
        int end_of_stylesheet = 0;
  
        // Lets just grab everything from the start up to the closing ?>
        for ( int i=start_of_stylesheet+1; (end_of_stylesheet==0)&&(i<250) ; i++ ) {
          if ( alert_bytes[i] == '?' ) {
            end_of_stylesheet = i;
          }
          else {
            // println("skip ${alert_bytes[i] as char} (${alert_bytes[i]} looking for ${quote_char})");
          }
        }
        if ( end_of_stylesheet != 0 ) {
          // Grab all the bytes up to the closing ?
          String stylesheet_temp = new String(Arrays.copyOfRange(alert_bytes, start_of_stylesheet+1, end_of_stylesheet))
          // try to extract the string matching href="anything" and drop anything into a group of it's own
          List matches = ( stylesheet_temp =~ /href="(.*)"/ ).findAll()
          if ( matches.size() > 0 ) {
            List groups = matches.getAt(0);
            if ( groups.size() > 1 )
              stylesheet = groups.getAt(1);
          }
        }
      }
    }
    catch ( Exception e ) {
      log.error("Unexpected error trying to extract stylesheet from ${cap_link} : ${e.getMessage()}",e);
    }
    return stylesheet;
  }

  private static int indexOf(byte[] data, byte[] pattern, max_search) {
    int[] failure = computeFailure(pattern);

    int j = 0;

    for (int i = 0; ( ( i < data.length ) && ( i < max_search ) ); i++) {
      while (j > 0 && pattern[j] != data[i]) {
        j = failure[j - 1];
      }
      if (pattern[j] == data[i]) { 
        j++; 
      }
      if (j == pattern.length) {
        return i - pattern.length + 1;
      }
    }
    return -1;
  }

  /**
   * Computes the failure function using a boot-strapping process,
   * where the pattern is matched against itself.
   */
  private static int[] computeFailure(byte[] pattern) {
    int[] failure = new int[pattern.length];

    int j = 0;
    for (int i = 1; i < pattern.length; i++) {
      while (j>0 && pattern[j] != pattern[i]) {
        j = failure[j - 1];
      }
      if (pattern[j] == pattern[i]) {
        j++;
      }
      failure[i] = j;
    }

    return failure;
  }
  
  def domNodeToString(node) {
    String xml_text =  groovy.xml.XmlUtil.serialize(node)
    xml_text
  }


  private void broadcastCapEvent(AlertWrapper aw, String feed_code) {

    log.debug("broadcastCapEvent : emit alerthub:CapEvent");
    try {

      // Emit this on it's way to CapEventHandlerService.process
      alertHubEventPublisher.publish(
        AlertHubEvent.builder()
          .eventCode('alerthub:CapEvent')
          .routingKey('CAPAlert.'+feed_code)
          .body(aw)
          .build());
    }
    catch ( Exception e ) {
      log.error("Problem trying to publish",e);
    }
  }

  private String generateAlertUUIDString(String cap_identifier) {
    String alert_uuid_str = null;
    try {
      log.info("generate uuid for ${cap_identifier}");
      UUID CAP_UUID = UUIDUtils.dnsUUID(CAP_NAMESPACE);
      UUID alert_uuid = UUIDUtils.nameUUIDFromNamespaceAndString(CAP_UUID, cap_identifier );
      alert_uuid_str = alert_uuid.toString();
      log.debug("ALERT-UUID: ${cap_identifier} == ${alert_uuid_str}")
    }
    catch ( Exception e ) {
      log.error("Problem in UUID genertion, e");
      throw(e);
    }
    return alert_uuid_str;
  }

	private List<String> validateCap(alerthub.cap.Alert pojoCap) {
		List<String> result = new java.util.ArrayList();

		if ( pojoCap == null ) {
			setAlarm(result,'NULL-POJO');
		}
		else {

			boolean polygon_found = false;

			for ( alerthub.cap.Info i : pojoCap.getInfo() ) {
				if ( ( i.getEvent() == null ) || ( i.getEvent().equalsIgnoreCase('unknown') ) ) 
					setAlarm(result,'UNKNOWN-EVENT');
				if ( ( i.getEvent() != null ) && ( i.getEvent().length() > 35 ) )
					setAlarm(result,'EVENT-TOO-LONG');
				if ( ( i.getUrgency() == null ) || ( i.getUrgency().equalsIgnoreCase('unknown') ) ) 
					setAlarm(result,'UNKNOWN-EVENT');
				if ( ( i.getSeverity() == null ) || ( i.getSeverity().equalsIgnoreCase('unknown') ) ) 
					setAlarm(result,'UNKNOWN-SEVERITY');
				if ( ( i.getCertainty() == null ) || ( i.getCertainty().equalsIgnoreCase('unknown') ) ) 
					setAlarm(result,'UNKNOWN-CERTAINTY');
				if ( ( i.getExpires() == null ) || ( i.getExpires().equalsIgnoreCase('unknown') ) || ( i.getExpires().trim().length() == 0 ) ) 
					setAlarm(result,'UNKNOWN-EXPIRES');
				for ( alerthub.cap.Area a : i.getArea() ) {
					if ( ( a.getAreaDesc() != null ) && ( a.getAreaDesc().length() > 50 ) ) 
						setAlarm(result,'AREA-DESC-TOO-LONG');
					if ( ( a.getPolygon() != null ) || ( a.getCircle() != null ) )
						polygon_found = true;
					if ( ( a.getCircle() != null ) && ( a.getCircle().trim().equals("0") ) )
						setAlarm(result,'ZERO-RADIUS-CIRCLE');
				}
			}

			if ( !polygon_found )
				setAlarm(result,'MISSING-EXPLICIT-LOCATION');

			if ( pojoCap.getSignature() != null )
						setAlarm(result,'SIGNED');
		}
		
		return result;
	}

	private void setAlarm(List<String> alarms, String alarm) {
		if ( ! alarms.contains(alarm) ) {
			alarms.add(alarm);
		}
	}

}
