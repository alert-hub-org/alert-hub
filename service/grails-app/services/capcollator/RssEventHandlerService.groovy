package capcollator;

import grails.gorm.transactions.*;
import static groovy.json.JsonOutput.*;
import grails.events.annotation.Subscriber;
import alerthub.services.pubsub.AlertHubEvent;
import java.util.Map;

@Transactional
class RssEventHandlerService {

  def capUrlHandlerService
  def feedFeedbackService

  @Subscriber('alerthub.RSSEntry')
  def handleNotification(AlertHubEvent evt) {
    log.info("RssEventHandlerService::handleNotification(${evt})");

    feedfacade.Entry body = evt.getBody().get("entry");
    Map headers = evt.getHeaders();

    if ( ( body == null ) || ( headers == null ) )
      throw new IllegalArgumentException("Body and headers cannot be null for Rss event - ${evt}");


    log.debug("Body: ${body}");
    log.debug("Headers: ${headers}");

    String source_feed = headers.get('feed-code');

    try {
      log.debug("*** calling handleNotification ${body.getLink()}");
      capUrlHandlerService.handleNotification(body.link, headers)
    }
    catch ( Exception e ) {
      log.error("problem handling cap alert (A) ${evt} ${e.message}",e);
      feedFeedbackService.publishFeedEvent(source_feed,
                                           null,
                                           "problem processing RSS event: ${e.message}");

    }

  }
}
