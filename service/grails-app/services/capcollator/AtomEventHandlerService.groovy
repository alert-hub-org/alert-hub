package capcollator

import grails.async.Promise;
import grails.gorm.transactions.*;
import static groovy.json.JsonOutput.*;
import static grails.async.Promises.*;
import grails.events.annotation.Subscriber;
import alerthub.services.pubsub.AlertHubEvent;

@Transactional
class AtomEventHandlerService {

  CapUrlHandlerService capUrlHandlerService
  FeedFeedbackService feedFeedbackService
  EventService eventService;

  private static long LONG_ALERT_THRESHOLD = 2000;

  @Subscriber('alerthub.ATOMEntry')
  def handleNotification(AlertHubEvent evt) {

    feedfacade.Entry body = evt.getBody().get("entry");
    Map headers = evt.getHeaders();

    log.info("alerthub.ATOMEntry event handled by AtomEventHandlerService::handleNotification(${evt}) ${body.getLink()}");

    if ( ( body == null ) || ( headers == null ) )
      throw new IllegalArgumentException("Body and headers cannot be null for Atom event - ${evt}");


    log.debug("Body: ${body}");
    log.debug("Headers: ${headers}");

    String source_feed = headers['feed-code'];

    try {
      log.debug("*** calling handleNotification ${body.getLink()}");
      capUrlHandlerService.handleNotification(body.getLink(), headers)
    }
    catch ( Exception e ) {
      log.error("problem handling cap alert ${evt} ${e.message}",e);
      feedFeedbackService.publishFeedEvent(source_feed,
                                           null,
                                           "problem processing Atom event: ${e.message}");

    }

    log.debug("ATOM Event Handler complete ${body.getLink()}");
  }
  
}
