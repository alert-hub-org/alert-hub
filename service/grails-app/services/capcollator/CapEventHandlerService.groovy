package capcollator

import alerthub.cap.Alert
import alerthub.cap.Info
import alerthub.services.pubsub.AlertHubEvent
import alerthub.services.pubsub.AlertHubEventPublisher
import alerthub.utils.AlertWrapper
import alerthub.utils.Snowflake
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import grails.core.GrailsApplication
import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional
import groovyx.net.http.ApacheHttpBuilder
import groovyx.net.http.ChainedHttpConfig
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.HttpClientBuilder
import org.w3c.dom.Document

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import java.text.SimpleDateFormat
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import java.lang.Math;
import alerthub.dto.WrappedSearchResult;

// @GrailsCompileStatic
@Transactional
class CapEventHandlerService {

  // Initialise MAX_HTTP_TIME to 20s, but this is no longer static. We plan to reduce this number as the number
  // of pending notifications increases - so that at times of high throughput, requests will fail faster to clear
  // any backlog
  private int MAX_HTTP_TIME = 10*1000; // 5s

  AlertHubEventPublisher alertHubEventPublisher;
  ESWrapperService ESWrapperService
  EventService eventService
  GazService gazService
  FeedFeedbackService feedFeedbackService
  GrailsApplication grailsApplication
  Snowflake snowflake;

  int active_tasks = 0;

  // import org.apache.commons.collections4.map.PassiveExpiringMap;
  // Time to live in millis - 1000 * 60 == 1m 
  // private Map geo_query_cache = Collections.synchronizedMap(new PassiveExpiringMap(1000*60))
  
  ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(8);

  private static final double EARTH_RADIUS_METERS = 6371000.0;

	public int getQueueDepth() {
		return executor.getQueue().size();
	}

  /**
   * Fired when we have detected a CAP event, to capture the event and index it in our local ES index
   * public void process(Map<String, Object> cap_notification) {
   */
  @Subscriber('alerthub:CapEvent')
  public void process(AlertHubEvent ahe) {

		try {
	    log.debug("CapEventHandlerService::process(${ahe})");
		  // log.debug("body: ${ahe.getBody()}");
			// log.debug("headers: ${ahe.getHeaders()}");

			// Scale max wait time if busy
	    if ( active_tasks > 15 )
				MAX_HTTP_TIME = 9000; // 

	    log.info("CapEventHandlerService::process - active tasks= ${++active_tasks} MaxTime=${MAX_HTTP_TIME} queue depth: ${executor.getQueue().size();}");

		  executor.execute {
				alerthub.Alert.withNewSession { session ->
					try {
						alerthub.Alert.withNewTransaction {
							internalProcess(ahe)
						}
					}
					catch( Exception e ) {
						log.error("CapEventHandlerService::process INNER ERROR",e);
					}
					finally {
						session.clear();
					}
				}
			}
		}
		catch ( Exception e) {
			log.error("CapEventHandlerService::process ERROR",e);
		}
  }

  def internalProcess(AlertHubEvent ahe) {

    AlertWrapper aw = (AlertWrapper) ahe.getBody();

    Map<String,Object> alert_body = aw.getAlertBody();
    Map<String,Object> alert_metadata = aw.getAlertMetadata();

    // break out this call here as this needs to be convered into an executor pool, this handler is
    // becoming a bottleneck in processing as alerts with high numbers of areas and high numbers of
    // vertices can slow down processing

    String alert_uuid = aw.getAlertMetadata().get('capCollatorUUID');

		if ( ( aw.getProcessingFlags() != null ) && ( aw.getProcessingFlags().contains("FORCE") ) ) {
			log.info("FORCE processing on uuid {}",alert_uuid);
		}
		else if ( alerthub.Alert.get(alert_uuid) != null ) {
			log.warn("Alert already present in DB. Decrement active tasks ${--active_tasks}");
			return;
		}

    long start_time = System.currentTimeMillis();
    log.info("CapEventHandlerService::process alert from ${alert_metadata.sourceFeed}) url=${alert_metadata.PrivateSourceUrl}");
    if ( alert_metadata.get('CCHistory') == null)
      alert_metadata.CCHistory = [];
    if ( alert_metadata.get('tags') == null )
      alert_metadata.put('tags',[]);
    if ( alert_metadata.get('warnings') == null )
      alert_metadata.put('warnings',[]);
    if ( alert_metadata.get('errorDetails') == null )
      alert_metadata.put('errorDetails',[]);

		// Add all alarms as tags
		alert_metadata.tags.addAll(aw.getAlarms());

    alert_metadata.CCHistory.add(['event':'CC-spatial-processing-start','timestamp':System.currentTimeMillis()]);
    alert_metadata.compound_identifier = new String(alert_metadata.sourceFeed+'|'+alert_body.identifier+'|'+alert_body.sent);

		if ( ( aw.getPojoCAP() == null ) || ( alert_body == null ) ) {
			log.error("internalProcess - PojoCAP not set  :${ahe} - decrement active tasks ${--active_tasks}");
			throw new RuntimeException("PojoCAP Null or alert_body null");
		}

		// String ym = aw.getPojoCAP().getSent()?.take(7);
		String ym = alert_body.sent.take(7);
 		alert_metadata.analysisym = ym;
 		alert_metadata.analysis_buckets = [ "c0-${ym}".toString() ];

    try {
      def polygons_found=0

      // changing this - use the system timestamp for ordering rather than the date on the alert - which can be odd due to
      // timezone offsets (Incorrectly formatted iso dates don't order properly if they have a timezone offset).
      SimpleDateFormat ts_sdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS'Z'".toString());
      ts_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
      aw.setEvtTimestampStr(ts_sdf.format(new Date()));

      Map geo_query_cache = [:]

      // Extract any shapes from the cap (info) alert['alert']['info'].each { it.['area'] }
      if ( alert_body?.info ) {
        List list_of_info_elements = alert_body.info instanceof List ? alert_body.info : [ alert_body.info ]

        // Create a set - this will prevent duplicate subscriptions if multiple info elements match
        Set<String> matching_subscriptions = new java.util.HashSet<String>()

        long size_ie_array = list_of_info_elements.size()
        long ie_ctr = 0;
        list_of_info_elements.each { ie ->

          long query_phase_start_time = System.currentTimeMillis();

            String event_code = ie.eventCode != null ? (ie.eventCode.value) : 'NOEVT';
            alert_metadata.analysis_buckets.add("c1-${ym}-${ie.category?:'NOCAT'}-${event_code}-${ie.urgency?:'NOURG'}-${ie.severity?:'NOSEV'}-${ie.certainty?:'NOCERT'}".toString())
            alert_metadata.analysis_buckets.add("c2-${ie.category?:'NOCAT'}-${event_code}-${ie.urgency?:'NOURG'}-${ie.severity?:'NOSEV'}-${ie.certainty?:'NOCERT'}".toString())
            alert_metadata.analysis_buckets.add("c3-${event_code}-${ie.urgency?:'NOURG'}-${ie.severity?:'NOSEV'}-${ie.certainty?:'NOCERT'}".toString())

          // log.debug("  -> Check info element ${ie_ctr}/${size_ie_array}");
          if ( ie.area ) {
            def list_of_area_elements = ie.area instanceof List ? ie.area : [ ie.area ]

            list_of_area_elements.each { area ->

              // log.debug("Processing area...");

              if ( area.cc_polys == null ) {
                area.cc_polys = [];
              }

              if ( ( area.polygon != null ) && ( ! ( area.polygon instanceof net.sf.json.JSONNull ) ) ) {

                if (!alert_metadata.tags.contains('AREATYPE_POLYGON'))
                  alert_metadata.tags.add('AREATYPE_POLYGON');

                def list_of_polygon_elements = area.polygon instanceof List ? area.polygon : [area.polygon]

                // log.debug("....As polygon.. count=${list_of_polygon_elements.size()}");

                list_of_polygon_elements.each { poly_elem ->

                  if (poly_elem != null) {
                      polygons_found++
                    // We got a polygon
                    def inner_polygon_ring = geoJsonToPolygon(poly_elem)
                    // log.info("Perform geo query : ${inner_polygon_ring}");

										if ( inner_polygon_ring.size() == 0 ) {
											log.error("No pairs found in inner polygon ring : ${aw.getUrl()}");
											addTag(alert_metadata.tags, 'GEO_SEARCH_ERROR/EMPTY_POLYGON');
                      alert_metadata.errorDetails.add("Empty polygon ${aw.getUrl()} / ${poly_elem}".toString());
										}
										else if ( inner_polygon_ring.size() < 4 ) {
											log.error("Insufficent Vertices :: Not enough pairs found in inner polygon ring ${aw.getUrl()}");
											log.error("Original ${poly_elem}");
											log.error("Processed ${inner_polygon_ring}");
											addTag(alert_metadata.tags, 'GEO_SEARCH_ERROR/INSUFFICIENT_VERTICES')
                      alert_metadata.errorDetails.add("Insufficent Vertices ${aw.getUrl()} / ${inner_polygon_ring.size()} / ${poly_elem} / ${inner_polygon_ring}".toString());
                    }
										else {
                      def match_result = matchSubscriptionPolygon(geo_query_cache ,inner_polygon_ring)

                      if ( match_result == null ) {
                        log.error("GEO Search for inner polygon ring returned null - ${inner_polygon_ring} / ${aw.getUrl()}");
                      }
                      else {
                        log.debug("Raw geo matches: ${match_result?.subscriptions?.size()}");
                        if ( match_result.subscriptions != null ) {
                          matching_subscriptions.addAll(filterNonGeoProperties(match_result.subscriptions, aw, ie));
                        }
                      }

                      alert_metadata['warnings'].addAll(match_result.messages);

                      if ( match_result.status == 'ERROR' ) {
                        log.warn("Adding GEO_SEARCH_ERROR tag to alert for ${aw.getUrl()}");
						  					addTag(alert_metadata.tags, 'GEO_SEARCH_ERROR/POLYGON');

                        // IF the search failed - that probably means that there is a problem with the source shape.
                        if ( alert_metadata.errorShapes == null )
                          alert_metadata.errorShapes = [ inner_polygon_ring.toString() ]
                        else
                          alert_metadata.errorShapes.add(inner_polygon_ring.toString())
  
                        alert_metadata.spatialErrorDetail = match_result.spatialErrorDetail
                      }
                      else {
                        // We enrich the parsed JSON document with a version of the polygon that ES can index to make the whole
                        // database of alerts geo searchable
                        area.cc_polys.add( [ type :'polygon', coordinates: [ inner_polygon_ring ] ] );
                      }
                    }
                  }
                  else {
                    log.warn("Polygon element found but children not present")
                  }
                }
  
                // If we got a polygon AND there was an info.area.geocode then we can look to see if we should cache that code
                // this is duff -- an alert can have many polygons and many geocodes, so the assumption here is wrong. 
                if ( 1==2 ) {
                  if ( area.geocode && area.geocode.value && area.geocode.valueName ) {
                    log.debug("CAP Alert contains polygon and geocode - cache value - ${area.geocode}");
                    def authorities = area.geocode.valueName instanceof List ? area.geocode.valueName : [ area.geocode.valueName ]
                    def symbols = area.geocode.value instanceof List ? area.geocode.value : [ area.geocode.value ]
    
                    Iterator i1=authorities.iterator()
                    Iterator i2=symbols.iterator()
                    for (; i1.hasNext() && i2.hasNext(); ) {
                      
                      try {
                        gazService.cache(i1.next(), i2.next(), inner_polygon_ring);
                      }
                      catch ( Exception e ) {
                        log.error("problem trying to cache gaz entry",e);
                      }
                    }
                  }
                }
  
                area.geocode?.each { gc ->
                  log.debug("CAP Alert has geocode : ${gc} ");
                }
              }

              if ( area.circle != null ) {

                // log.debug("....As circle");

                if ( !alert_metadata.tags.contains('AREATYPE_POLYGON') )
                  alert_metadata.tags.add('AREATYPE_CIRCLE');

                def list_of_circle_elements = area.circle instanceof List ? area.circle : [ area.circle ]

                list_of_circle_elements.each { circle ->
                  polygons_found++
  
                  // log.debug("Area defines a circle"); // EG 2.58,-70.06 13
                  def coords_radius = circle.split(' ');
                  def coords = null;
                  def radius = null;
  
                  if ( coords_radius.length > 0 ) {
                    coords = coords_radius[0].split(',');
                  }
  
                  if ( coords_radius.length > 1 ) {
                    // CAP Standard determies that radius must be given in KM, so no conversion needed.
                      // LEFT HERE FOR INFO:: THIS IS NOT THE CASE NOW Got radius part - We assume the feed is giving a radius in miles, so we convert to ES KM here // radius = Integer.parseInt(coords_radius[1]) * 1.6;
                    radius = Float.parseFloat(coords_radius[1])
                  }
                  else {
                    // log.debug("Using default radius of 10 KM");
                    radius = 0
                  }
  
                  if ( coords != null ) {
                    if ( radius == 0 ) {
                      def lat = Float.parseFloat(coords[0]);  // -90 to +90
                      def lon = Float.parseFloat(coords[1]);  // -180 to +180
                      def match_result = matchSubscriptionAsPoint(lat,lon);
                      matching_subscriptions.addAll(filterNonGeoProperties(match_result.subscriptions, aw, ie));
                      alert_metadata['warnings'].addAll(match_result.messages);
                      if ( match_result.status == 'ERROR' ) {
												addTag(alert_metadata.tags, 'GEO_SEARCH_ERROR/CIRCLE_AS_POINT');
                      }
                      else {
                        area.cc_polys.add( [ type:'point', coordinates: [ lon, lat ] ] );
                      }
                    }
                    else {
                      // log.debug("Parse coordinates - we assume <circle> elements are composed within the circle element as lat <comma> lon <space> radius : ${circle}");
                      def lat = Float.parseFloat(coords[0]);  // -90 to +90
                      def lon = Float.parseFloat(coords[1]);  // -180 to +180
                      def match_result = matchSubscriptionCircleAsPolygon(lat,lon,radius)
                      matching_subscriptions.addAll(filterNonGeoProperties(match_result.subscriptions, aw, ie));
                      // matching_subscriptions.addAll(match_result.subscriptions);
    
                      alert_metadata['warnings'].addAll(match_result.messages);
    
                      if ( match_result.status == 'ERROR' ) {
												addTag(alert_metadata.tags, 'GEO_SEARCH_ERROR/CIRCLE_AS_POLY');
                        if ( alert_metadata.errorShapes == null )
                          alert_metadata.errorShapes = [ match_result.approximated_poly.toString() ]
                        else
                          alert_metadata.errorShapes.add(match_result.approximated_poly.toString() )

                        alert_metadata.spatialErrorDetail = match_result.spatialErrorDetail
                      }
                      else {
                        // We enrich the parsed JSON document with a version of the polygon that ES can index to make the whole
                        // database of alerts geo searchable
   
                        // Convert the circle to a polygon
                        // area.cc_polys.add(  [ type:'circle', coordinates:[ coords[1], coords[0] ], radius:"${radius}km".toString() ] )
                        //double[][] approximated_poly = circle2polygon(10, lat, lon, radius)
                        area.cc_polys.add( [ type:'polygon', coordinates: [ match_result.approximated_poly ] ] );
                      }
                    }
                  }
                  else {
                    log.error("Failed to parse circle area ${circle}");
                  }
                }
              }
            }
          }
          log.debug("info element checking complete${ie_ctr++}/${size_ie_array}. QueryPhase elapsed: ${System.currentTimeMillis() - query_phase_start_time}");
        }

        log.debug("The following subscriptions matched : ${matching_subscriptions} (# polygons found:${polygons_found})");

        if ( polygons_found == 0 ) {
          eventService.registerEvent('CAPXMLWithNoPolygon',System.currentTimeMillis());
         alert_metadata.tags.add('No_Polygon_Provided');
        }
        else {
          alert_metadata.tags.add('Mappable');
        }

        if ( ! matching_subscriptions.contains('unfiltered') ) {
          // It's likely that the alert was well formed, but did not contain a geo element, and therefore
          // did not match the unfiltered subscription. Add it anyway!
          matching_subscriptions.add('unfiltered');
        }

        alert_metadata.CCHistory.add(['event':'CC-spatial-processing-complete','timestamp':System.currentTimeMillis()]);

        saveAlertInArchive(ahe, matching_subscriptions);

        log.debug("Calling publish alert");
        publishAlert(ahe, matching_subscriptions);

        log.debug("Calling index alert");
        // Index the CAP event
        indexAlert(ahe, matching_subscriptions)

        // If there was a geo search error, send feedback to the feed fetcher that there is an issue with the alerts
        // coming from this feed.
        if ( alert_metadata.tags.find { it.startsWith('GEO_SEARCH_ERROR') } != null ) {
          feedFeedbackService.publishFeedEvent((String) alert_metadata.sourceFeed,
                                               null,
                                               [ key: "GEO_SEARCH_ERROR / ${alert_metadata.compound_identifier}",
                                                 message: alert_metadata.spatialErrorDetail,
                                                 matchedSubscriptions:matching_subscriptions,
                                                 tags:alert_metadata.tags,
                                                 history:alert_metadata.CCHistory] );

          if ( grailsApplication.config.fah.MQTTDeliver == 'on' ) {
            log.debug("Delivering to amq.topic exchange");
            alertHubEventPublisher.publish(
              AlertHubEvent.builder()
                .routingKey("GEO_SEARCH_ERROR")
                .body([ key: "GEO_SEARCH_ERROR / ${alert_metadata.compound_identifier}",
                       message: alert_metadata.spatialErrorDetail,
                       matchedSubscriptions:matching_subscriptions,
                       tags:alert_metadata.tags,
                       history:alert_metadata.CCHistory])
                .build());
          }
        }
      }
      else {
        if ( alert_metadata.sourceFeed != null ) {
          feedFeedbackService.publishFeedEvent((String)(alert_metadata.sourceFeed),
                  'internalProcess',
                  [ key:"NO_INFO_ELEMENT / ${alert_metadata.compound_identifier}",
                    message: "Unable to find any INFO element in alert XML"] );
        }
        else {
          log.error("NO ALERT BODY")
        }
      }

    }
    catch ( java.net.SocketTimeoutException e ) {
      log.error("CapEventHandlerService::internalProcess Exception processing CAP notification: ${e.message}\nbody:${alert_body}\nmetadata:${alert_metadata}\n");
      feedFeedbackService.publishFeedEvent(alert_metadata.sourceFeed,null,[ key:'NETWORK_ERROR_FETCHING_ALERT', message: "Error: ${e.message}".toString() ])
    }
    catch ( Exception e ) {
      log.error("CapEventHandlerService::internalProcess Exception ${e.message} processing CAP notification:\nbody:${alert_body}\nmetadata:${alert_metadata}\n",e);
      feedFeedbackService.publishFeedEvent(alert_metadata.sourceFeed,null,[ key:'GENERAL_EXCEPTION_PROCESSING_ALERT', message: "Error: ${e.message}".toString() ])
    }
    finally {
      log.info("CapEventHandlerService::internalProcess complete elapsed=${System.currentTimeMillis() - start_time}. active tasks=${--active_tasks} ${alert_metadata?.tags}");
    }
  }


  private void saveAlertInArchive(AlertHubEvent ahe, Set<String> matching_subscriptions) {

    AlertWrapper aw = ahe.getBody() as AlertWrapper;

    String alert_uuid = aw.getAlertMetadata().get('capCollatorUUID');
    String snowflake_id = aw.getAlertMetadata().get('snowflakeId');

    log.debug("store alert with native identifier ${aw.getPojoCAP()?.getIdentifier()} resolved as internal ${alert_uuid} - snowflake is ${snowflake_id}");

    Alert a = aw.getPojoCAP();

    if ( a == null )
      throw new RuntimeException("Parsed CAP was NULL ${alert_uuid} = Raw was ${new String(aw.getRawCAP())}");

		// Double check that there is no existing record 
		if ( alerthub.Alert.get(alert_uuid) != null ) {
			log.warn("Alert ${alert_uuid} already present in DB - not saving");
			return;
		}

    alerthub.Alert db_record = new alerthub.Alert(
      id: alert_uuid,
      snowflake: snowflake_id,
      identifier: a.getIdentifier()?.take(512),
      content: null,
      sender: a.getSender()?.take(255),
      sent: a.getSent()?.take(255),
      status: a.getStatus()?.take(255),
      msgType: a.getMsgType()?.take(255),
      source: a.getSource()?.take(255),
      restriction: a.getRestriction()?.take(255),
      addresses: a.getAddresses()?.take(255),
      code : a.getCode()?.take(255),
      note: a.getNote()?.take(255),
      references: a.getReferences()?.take(512),
      incidents: a.getIncidents()?.take(255),
      scope: a.getScope()?.take(255),
      rawAlert: new String(aw.getRawCAP()),
      url: aw.getUrl(),
      analysisYearMonth: a.getSent().substring(0,6),
      analysisFull: null
    );

    db_record.save(flush:true, failOnError:true);
  }

  def publishAlert(AlertHubEvent ahe, Set<String> matching_subscriptions) {

    AlertWrapper aw = ahe.getBody() as AlertWrapper;
    Map<String,Object> alert_metadata = aw.getAlertMetadata();
    Map<String,Object> alert_body = aw.getAlertBody();


    log.debug("publishAlert - Publishing CAPSubMatch. ${matching_subscriptions}");

    if ( alert_metadata.PrivateSourceUrl != null )
      alert_metadata.remove('PrivateSourceUrl');

    matching_subscriptions.each { sub_id ->

      // Apply other subscription filters
      try {
        if ( criteriaMet(sub_id, aw) ) {
          log.debug("Publishing CAPSubMatch.${sub_id} notification");

          alertHubEventPublisher.publish (
            AlertHubEvent.builder()
              .eventCode('CAPSubMatch')
              .routingKey('CAPSubMatch.'+sub_id)
              .body(aw)
              .build());
        }
      }
      catch ( Exception e ) {
        log.error("Problem trying to publish to pubsub",e);
      }
    }
  }

  private boolean criteriaMet(String sub_id, AlertWrapper aw) {
    return true;
  }

  private void indexAlert(AlertHubEvent ahe, Set<String> matching_subscriptions) {

    AlertWrapper aw = ahe.getBody() as AlertWrapper;

    Map<String,Object> alert_metadata = aw.getAlertMetadata();
    Map<String,Object> alert_body = aw.getAlertBody();

    Map<String,Object> alert_index_record = [
            AlertBody: alert_body,
            AlertMetadata: alert_metadata,
            AlertTimestamp: aw.getEvtTimestampStr()
    ];

    String alerts_live_index = grailsApplication.getConfig().getProperty('indexes.AlertsLive.name');
    String alerts_archive_index = grailsApplication.getConfig().getProperty('indexes.AlertsArchive.name');

    log.info("INDEX ALERT.....live index: ${alerts_live_index} archive:${alerts_archive_index}");

    if ( alert_metadata ) {
			try {
        // Store the matching subscriptions in the metadata
        alert_metadata['MatchedSubscriptions']=matching_subscriptions

        // Drop the signature -- it's very verbose and applies to the underlying XML document. 
        // Consumers should return the source CAP if they want to validate the alert

        alert_body.Signature=null

        String internal_identifier = alert_body.get('identifier');
        if ( ( internal_identifier != null  ) && ( internal_identifier.length() > 0 ) ) {
          // Previously we tried to index preferentially by alert_metadata.compound_identifier but now alert_body.identifier is the way

          // Convert the description to a string if it's not already (Suggestive of HTML markup inside the description element);

          // until we finally expunge the grails XML parser, ensure we have a list of info elements
          List list_of_info_elements = alert_index_record.AlertBody.info instanceof List ? alert_index_record.AlertBody.info : [ alert_index_record.AlertBody.info ]

          list_of_info_elements.each { info ->
            if ( ( info.descriptopm != null ) &&
                 ( ! ( info.description instanceof String ) ) ) {
              log.warn("Converting description ${info.description} to string")
              info.description = "${info.description}".toString();
            }
          }

          log.debug("Examine ${alert_metadata.Expires}");
          //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ssZ".toString());
          //  long expires_in_millis = alert_metadata.Expires != null ? sdf.parse(alert_metadata.Expires) : 0;
          ESWrapperService.doIndex(alerts_live_index, '_doc', internal_identifier, alert_index_record)
          ESWrapperService.doIndex(alerts_archive_index, '_doc', internal_identifier, alert_index_record)
        }
        else {
          log.error("alert_body.identifier is null or empty")
        }
      }
      catch ( co.elastic.clients.elasticsearch._types.ElasticsearchException ese ) {
				log.error("Failed to index record",ese);
				log.error("Exception processing Source URL: ${aw.getUrl()} ${ese.getMessage()}");
				log.error("index doc: ${alert_index_record}");
			}
    }
  }

	// Convert a string of format "lat,lon lat,lon lat, lon" to an array of [lon,lat] pairs in a list of form
  // [ [lon, lat], [lon,lat], [lon,lat] ]
  private List<List<Float>> geoJsonToPolygon(String polygon_ring_string) {


    // Polygon as given is a ring list of space separated pairs - "x1,y1 x2,y2 x3,y3 x4,y4 x1,y1"
    List polygon_ring = []

    def last_pair = null
    // def cleaned_polygon_ring_string = polygon_ring_string.replaceAll('\\s+',' ');
    def cleaned_polygon_ring_string = polygon_ring_string.replaceAll('\n',' ');
    def list_of_pairs = cleaned_polygon_ring_string.split(' ')
    list_of_pairs.each { coordinate_pair ->
      // geohash wants lon,lat the other way to our geojson, so flip them
      def split_pair = coordinate_pair.split(',')
      if ( split_pair.size() == 2 ) {
        if ( ( last_pair != null ) && ( ( last_pair[0] == split_pair[0] ) && ( last_pair[1] == split_pair[1] ) ) ) {
          // log.debug("Skipping repeated pair of coordinates ${split_pair}");
        }
        else {
          polygon_ring.add([Float.parseFloat(split_pair[1]),Float.parseFloat(split_pair[0])])
          last_pair = split_pair
        }
      }
      else {
        log.error("Problem attempting to split coordiate pair ${coordinate_pair}");
      }
    }

		int polygon_ring_vertex_count = polygon_ring.size();

		if ( polygon_ring_vertex_count > 3 ) {
			List<Float> first_vertex = polygon_ring[0]
			List<Float> last_vertex = polygon_ring[polygon_ring_vertex_count-1]
			if ( ( first_vertex[0] == last_vertex[0] ) && ( first_vertex[1] == last_vertex[1] ) ) {
				// Golden - closed polygon
			}
			else {
				log.warn("Polygon is not closed... Closing it");
				polygon_ring.add(first_vertex.clone());
			}
		}
		else {
			log.warn("Invalid vertex count ${polygon_ring_string}");
		}

    return polygon_ring
  }


  def matchSubscriptionAsPoint(lat, lon) {

    def result = [
      subscriptions:null,
      messages:[],
      status:'OK'
    ]

    String query = '''{ "query": { "bool": { "must": { "match_all": {} }, "filter": { "geo_shape": { "subshape": { "shape": { "type": "point", "coordinates":['''+lon+''','''+lat+'''] }, "relation":"intersects" } } } } } }'''
    String subscriptions_index = grailsApplication.getConfig().getProperty('indexes.AlertsSubscripions.name');

    try {
      WrappedSearchResult matching_subs = ESWrapperService.searchJson(subscriptions_index,query,0,200,null,null);

      if ( matching_subs ) {
        result.subscriptions = matching_subs.getHits();
      }
    }
    catch ( Exception e ) {
      result.messages.add((e.message+'\n'+query).toString());
      result.status='ERROR';
      log.error("[point] SEARCH ERROR(${e.message}):: Validate with\ncurl -X GET 'http://eshost:9200/${subscriptions_index}/_search' -H 'Content-Type: application/json' -d '${query}'")
    }

    result
  }

  /**
   * Find all subscriptions which overlap with the supplied polygon ring
   * having obtained the list, check non-spatial filter properties
   */
  Map<String,Object> matchSubscriptionPolygon(geo_query_cache,polygon_ring) {

    // log.debug("matchSubscriptionPolygon(${polygon_ring})");

    String poly_str = polygon_ring.toString();

    Map<String,Object> result = geo_query_cache.get(poly_str);

    if ( result != null ) {
      return result;
    }
    else {
      result = [
        subscriptions:null,
        messages:[],
        status:'OK'
      ]
    }

    String query = '''{ "query": { "bool": { "must": { "match_all": {} }, "filter": { "geo_shape": { "subshape": { "shape": { "type": "polygon", "coordinates":['''+polygon_ring+'''] }, "relation":"intersects" } } } } } }'''

    String subscriptions_index = grailsApplication.getConfig().getProperty('indexes.AlertsSubscripions.name');
    try {
      def matching_subs = ESWrapperService.searchJson(subscriptions_index ,query,0,200,null,null);

      if ( matching_subs ) {
        result.subscriptions = matching_subs.hits;
      }
      else{
        log.error("ESWrapperService.searchJson returned NULLi - query:${query} against ${subscriptions_index}");
      }
    }
    catch ( Exception e ) {
      result.messages.add((e.message+'\n'+query).toString());
      result.subscriptions = [];
      result.status='ERROR';
      log.error("[polygon] SEARCH ERROR(${e.message}):: Validate with\ncurl -X GET 'http://eshost:9200/${subscriptions_index}/_search' -H 'Content-Type: application/json' -d '${query}'", e)
    }
    finally {
      log.debug("search completed ok");
    }

    geo_query_cache.put(poly_str,result)

    result
  }

  private List<String> filterNonGeoProperties(List<co.elastic.clients.elasticsearch.core.search.Hit> matching_subscriptions,
                                      AlertWrapper aw,
                                      Map info_element) {

    Map<String,Object> alert_metadata = aw.getAlertMetadata();
    List<String> result = new ArrayList<String>()

    // log.debug("filterNonGeoProperties(...,${aw} ${info_element})");

    // In CapUrl handler we might have decorated the the alert URL with some credentials and
    // put that alert in PrivateSourceUrl. Reuse it here. This URL will be removed before the
    // json is published so we don't leak credential
    Document d = fetchDOM(alert_metadata.PrivateSourceUrl);
    ObjectMapper mapper = new ObjectMapper();

    if ( matching_subscriptions != null ) {
      matching_subscriptions.each { matching_sub ->
        com.fasterxml.jackson.databind.node.ObjectNode sub_obj = matching_sub.source();
        Map<String, Object> sub_as_map = mapper.convertValue(sub_obj, new TypeReference<Map<String, Object>>(){});

        log.debug("sub as map: ${sub_as_map}");
        if ( passNonSpatialFilter(sub_as_map, aw, info_element, d) ) {
          result.add(sub_as_map.get('shortcode')?.toString())
        }
      }
    }

    log.debug("filterNonGeoProperties called with list of ${matching_subscriptions?.size()} returns list of ${result?.size()}");
    return result;
  }

  def matchSubscriptionCircle(float lat, float lon, float radius) {

    // log.debug("matchSubscriptionCircle(${lat},${lon},${radius})");

    def result=[
      subscriptions:null,
      messages:[],
      status:'OK'
    ]

    String query = '''{ "query": { "bool": { "must": { "match_all": {} }, "filter": { "geo_shape": { "subshape": { "shape": { "type": "circle", "coordinates":['''+lon+''','''+lat+'''], "radius": "'''+radius+'''km" }, "relation":"intersects" } } } } } }'''


    String subscriptions_index = grailsApplication.getConfig().getProperty('indexes.AlertsSubscripions.name');
    try {
      def matching_subs = ESWrapperService.searchJson(subscriptions_index,query,0,200,null,null);

      if ( matching_subs ) {
        result.subscriptions = matching_subs.hits
      }
    }
    catch ( Exception e ) {
      result.messages.add((e.message+'\n'+query).toString());
      log.error("Problem trying to match circle::${e.message}",e);
      result.status='ERROR';
      log.error("matchSubscriptionCircle ERROR: Validate with\ncurl -X GET 'http://eshost:9200/${subscriptions_index}/_search' -H 'Content-Type: application/json' -d '${query}'")
    }

    result
  }

  /**
   * Find all subscriptions which overlap with the supplied polygon ring
   * having obtained the list, check non-spatial filter properties
   */
  def matchSubscriptionCircleAsPolygon(float lat, float lon, float radius) {

    log.debug("matchSubscriptionCircleAsPolygon(${lat},${lon},${radius},...)");

    double[][] poly = circle2polygon(10, lat, lon, radius) ;

    log.debug("poly: ${poly}");

    def result = [
      approximated_poly: poly,
      subscriptions:null,
      messages:[],
      status:'OK'
    ]

    String query = '''{ "query" : { "bool": { "must": { "match_all": {} }, "filter": { "geo_shape": { "subshape": { "shape": { "type": "polygon", "coordinates":['''+poly.toString()+'''] }, "relation":"intersects" } } } } } }'''


    String subscriptions_index = grailsApplication.getConfig().getProperty('indexes.AlertsSubscripions.name');
    try {
      def matching_subs = ESWrapperService.searchJson(subscriptions_index,query,0,200,null,null);

      if ( matching_subs ) {
        result.subscriptions = matching_subs.hits;
      }
    }
    catch ( Exception e ) {
      log.error("Problem in matchSubscriptionCircleAsPolygon(${lat},${lon},${radius})");
      result.messages.add((e.message+'\n'+query).toString());
      result.status='ERROR';
      log.error("[circleAsPoly] SEARCH ERROR(${e.message}):: Validate with\ncurl -X GET 'http://eshost:9200/${subscriptions_index}/_search' -H 'Content-Type: application/json' -d '${query}'", e)
    }

    result
  }


  private boolean passNonSpatialFilter(Map subscription, AlertWrapper aw, Map info_element, Document d) {

    boolean result = true;
    Map<String,Object> alertMetadata = aw.getAlertMetadata();

    if ( subscription.languageOnly != null ) {
      if ( !subscription.languageOnly?.equalsIgnoreCase('none') ) {
        def lang_from_alert =  info_element.language?.toLowerCase() ?: 'en'
        if ( lang_from_alert?.startsWith(subscription.languageOnly.toLowerCase()) ) {
          log.debug("Pass language-only");
        }
        else {  
          log.debug("Did not pass language filter (req:${subscription.languageOnly}/rejected:${info_element.language}) ");
          result = false;
        }
      }
    }

    // (//cap:urgency='Immediate' or //cap:urgency='Expected') and (//cap:severity='Extreme' or //cap:severity='Severe') and (//cap:certainty='Observed' or //cap:certainty='Likely')
    if ( subscription.highPriorityOnly != null ) {
      if ( subscription.highPriorityOnly?.equalsIgnoreCase('true') ) {
        // If ( urgency==immediate || urgency==expected ) && ( severity==extreme || severity==severe ) && ( certainty==observed || certainty==likely )
        // if ( info_element.urgency ) (info_element.severity)   info_element.certainty
        if ( ( info_element.urgency?.equalsIgnoreCase('immediate') || info_element.urgency?.equalsIgnoreCase('expected') ) &&
             ( info_element.severity?.equalsIgnoreCase('extreme') || info_element.severity?.equalsIgnoreCase('severe') ) &&
             ( info_element.certainty?.equalsIgnoreCase('observed') || info_element.severity?.equalsIgnoreCase('likely') ) ) {
          log.debug("Pass - Filter high priority only");
        }
        else {
          log.debug("Did not pass high priority filter - urgency:${info_element.urgency} severity:${info_element.severity} certainty:${info_element.certainty}");
          result = false;
        }
      }
    }

    if ( subscription.officialOnly != null ) {
      if ( subscription.officialOnly?.equalsIgnoreCase('true') ) {
        if ( ( alertMetadata.sourceIsOfficial != null ) && ( alertMetadata.sourceIsOfficial.equalsIgnoreCase('true') ) ) {
          log.debug("Pass Filter official priority only");
        }
        else {
          log.debug("Did not pass official filter source (${alertMetadata.sourceFeed}) isOfficial == ${alertMetadata.sourceIsOfficial} needs to == true to pass)");
          result = false;
        }
      }
    }

    // subscription.xPathFilter contains an expath expression
    // http://www.saxonica.com/documentation/#!xpath-api/jaxp-xpath/factory
    if ( ( subscription.xPathFilter != null ) && 
         ( subscription.xPathFilter.length() > 0 ) && 
         ( subscription.xPathFilter != 'none' ) ) {

      javax.xml.namespace.NamespaceContext ns_ctx = new javax.xml.namespace.NamespaceContext() {
         @Override
         public String getNamespaceURI(String prefix) {
           if ( prefix=='cap' )
             return 'urn:oasis:names:tc:emergency:cap:1.2'
           else
             return null;
         }
 
         @Override
         public String getPrefix(String namespaceURI) {
             return null;
         }
 
         @SuppressWarnings("rawtypes")
         @Override
         public Iterator getPrefixes(String namespaceURI) {
             return null;
         }
      }

      log.debug("Process xpath filter ${subscription.xPathFilter}");
      XPathFactory xpathFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
      XPath xPath = xpathFactory.newXPath();
      xPath.setNamespaceContext(ns_ctx)
      result = xPath.compile(subscription.xPathFilter).evaluate(d, XPathConstants.BOOLEAN);
      log.debug("XPath result: ${result}");
    }
    else {
      log.debug("No XPATH present (${subscription.xPathFilter})");
    }

    log.debug("passNonSpatialFilter against sub ${subscription?.shortcode} for ${aw} filter ${result?'':'Did not pass'} - returns ${result}");

    return result;
  }

  def matchAlertCircle(float lat, float lon, float radius) {

    def result = [messages:[]];
    log.debug("matchAlertCircle(${lat},${lon},${radius})");

    // ES shape points accept Geo-point expressed as an array with the format: [ lon, lat] or a string "lat,lon"
    String query = '''{
         "query": {
           "bool": {
             "must": {
               "match_all": {}
             },
             "filter": {
               "geo_shape": {
                 "AlertBody.info.area.cc_polys": {
                   "shape": {
                     "type": "circle",
                     "coordinates":['''+lon+''','''+lat+'''],
                     "radius": "'''+radius+'''km"
                   },
                   "relation":"intersects"
                 }
               }
             }
           }
         }
     }'''

    log.debug("Validate with\ncurl -X GET 'http://eshost:9200/alerts/_search' -H 'Content-Type: application/json' -d '${query}'")

    String[] indexes_to_search = [ 'alerts' ]
    try {
      result.alerts = ESWrapperService.searchJson(indexes_to_search,query,0,200,null,null);
      result.status='OK';
    }
    catch ( Exception e ) {
      log.error("Problem trying to match circle::${e.message}",e);
      result.messages.add(e.message);
      result.status='ERROR';
    }

    result
  }


  public static double[][] circle2polygon(int segments, double latitude, double longitude, double radius) {

    double PI = 3.141592;

    if (segments < 5) {
        throw new IllegalArgumentException("you need a minimum of 5 segments");
    }
    double[][] points = new double[segments+1][0];

    double relativeLatitude = radius / EARTH_RADIUS_METERS * 180 / PI;

    // things get funny near the north and south pole, so doing a modulo 90
    // to ensure that the relative amount of degrees doesn't get too crazy.
    double relativeLongitude = relativeLatitude / Math.cos(Math.toRadians(latitude)) % 90;

    for (int i = 0; i < segments; i++) {
        // radians go from 0 to 2*PI; we want to divide the circle in nice
        // segments
        double theta = 2 * PI * i / segments;
        // trying to avoid theta being exact factors of pi because that results in some funny behavior around the
        // north-pole
        theta = theta += 0.1;
        if (theta >= 2 * PI) {
            theta = theta - 2 * PI;
        }

        // on the unit circle, any point of the circle has the coordinate
        // cos(t),sin(t) where t is the radian. So, all we need to do that
        // is multiply that with the relative latitude and longitude
        // note, latitude takes the role of y, not x. By convention we
        // always note latitude, longitude instead of the other way around
        double latOnCircle = latitude + relativeLatitude * Math.sin(theta);
        double lonOnCircle = longitude + relativeLongitude * Math.cos(theta);
        if (lonOnCircle > 180) {
            lonOnCircle = -180 + (lonOnCircle - 180);
        } else if (lonOnCircle < -180) {
            lonOnCircle = 180 - (lonOnCircle + 180);
        }

        if (latOnCircle > 90) {
            latOnCircle = 90 - (latOnCircle - 90);
        } else if (latOnCircle < -90) {
            latOnCircle = -90 - (latOnCircle + 90);
        }

        points[i] = new double[2]// [ latOnCircle, lonOnCircle ];

        // For ES, we need lon,lat in the shape so we switch the original order here
        points[i][1] = latOnCircle
        points[i][0] = lonOnCircle
    }
    // should end with same point as the origin
    points[points.length-1] = new double[2]// [points[0][0],points[0][1]];
    points[points.length-1][0] = points[0][0]
    points[points.length-1][1] = points[0][1]
    return points;
  }

  // https://www.baeldung.com/java-xpath
  Document fetchDOM(String alert_url) {

    Document result = null;

    HttpBuilder http_client = ApacheHttpBuilder.configure {
      request.uri = alert_url
      request.headers['Accept'] = 'application/cap+xml, application/xml, text/xml'
      client.clientCustomizer { HttpClientBuilder builder ->
        RequestConfig.Builder requestBuilder = RequestConfig.custom()
        requestBuilder.connectTimeout = MAX_HTTP_TIME
        requestBuilder.connectionRequestTimeout = MAX_HTTP_TIME
        requestBuilder.socketTimeout = MAX_HTTP_TIME;
        builder.defaultRequestConfig = requestBuilder.build()
      }
    }

    String response_content = http_client.get {
      response.parser('application/cap+xml') { ChainedHttpConfig cfg, FromServer fs ->
        fs.inputStream.text
      }
      response.parser('application/xml') { ChainedHttpConfig cfg, FromServer fs ->
        fs.inputStream.text
      }
      response.parser('application/octet-stream') { ChainedHttpConfig cfg, FromServer fs ->
        fs.inputStream.text
      }
      response.parser('text/xml') { ChainedHttpConfig cfg, FromServer fs ->
        fs.inputStream.text
      }

      response.failure { FromServer resp ->
        log.debug("Failure fetching content : ${resp}")
        return null;
      }
    }

    if ( response_content ) {
      log.debug("Fetch alert from ${alert_url}");
      byte[] content_bytes = response_content.getBytes();
      InputStream is = new ByteArrayInputStream(content_bytes)
      DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = builderFactory.newDocumentBuilder();
      result = builder.parse(is);
    }

    return result;
  }

	private void addTag(List<String> l, String t) {
		if ( ! l.contains(t) )
			l.add(t);
	}
}
