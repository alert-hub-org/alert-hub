package capcollator

import co.elastic.clients.elasticsearch._types.query_dsl.Query
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.SortOptions;

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule

import java.text.SimpleDateFormat
import java.net.InetAddress;

import org.apache.http.HttpHost;
import static groovy.json.JsonOutput.*;

import groovy.util.logging.Slf4j
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.elasticsearch.client.RequestOptions;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.mapping.TypeMapping;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.InfoResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.DeleteRequest
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.IndexRequest
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.DeleteByQueryRequest;
import co.elastic.clients.elasticsearch.core.DeleteByQueryResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.bulk.BulkOperation;
import co.elastic.clients.elasticsearch.core.bulk.IndexOperation;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.transport.endpoints.BooleanResponse;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import co.elastic.clients.json.JsonpMapper;
import co.elastic.clients.elasticsearch._types.query_dsl.WrapperQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;

import org.apache.http.client.CredentialsProvider;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


import grails.core.GrailsApplication;
import jakarta.json.spi.JsonProvider;
import jakarta.json.stream.JsonParser;
import org.springframework.core.io.ClassPathResource;

import alerthub.dto.WrappedSearchResult;

/**
 * This service insulates KB+ from ES API changes - gather all ES calls in here and proxy them with local interface defintions
 * see https://www.elastic.co/blog/switching-from-the-java-high-level-rest-client-to-the-new-java-api-client
 */
@Slf4j
class ESWrapperService {

  static transactional = false;

  GrailsApplication grailsApplication;
  String es_host_name = null;
  String es_user = null;
  String es_pass = null;

  // https://artifacts.elastic.co/javadoc/co/elastic/clients/elasticsearch-java/8.0.1/co/elastic/clients/elasticsearch/ElasticsearchClient.html
  ElasticsearchClient esclient = null;

  ObjectMapper om = null;

  private static class SFJSONNullSerializer extends JsonSerializer<net.sf.json.JSONNull> {
    @Override
    public void serialize(net.sf.json.JSONNull value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
      gen.writeNull();
    }
  }

  public ESWrapperService() {
    this.om = new ObjectMapper()
      .setSerializationInclusion(JsonInclude.Include.NON_NULL);
    SimpleModule module = new SimpleModule();
    module.addSerializer(net.sf.json.JSONNull.class, new SFJSONNullSerializer());
    om.registerModule(module);
  }


  @javax.annotation.PostConstruct
  public void init() {
    es_host_name = grailsApplication.config.getProperty('eshost', 'http://localhost:9200');
    es_user = grailsApplication.config.getProperty('esuser', 'elastic');
    es_pass = grailsApplication.config.getProperty('espass', 'elastic');

    log.info("init ES wrapper service eshost: ${es_host_name}");
  }

  private ElasticsearchClient ensureClient(String elasticsearchServiceAddress) {

    // See: https://gist.github.com/bvader/491e2760d3010e028797b832e47fcecb
    try {
      final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
      credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(es_user, es_pass));

      log.debug("connecting to ${elasticsearchServiceAddress} as ${es_user}/${es_pass}");

      RestClient restClient = RestClient.builder(HttpHost.create(elasticsearchServiceAddress))
          .setHttpClientConfigCallback(hcb -> hcb
						.setDefaultCredentialsProvider(credentialsProvider)
						.setMaxConnTotal(20) // Maximum total connections
            .setMaxConnPerRoute(10)
            .setKeepAliveStrategy((response, context) -> 20000l)
          )
          .build();

      // Create the transport with a Jackson mapper
      ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());

      ElasticsearchClient client = new ElasticsearchClient(transport);

      InfoResponse info = client.info();

      log.info("Connected to a cluster running version {} at {}.", info.version().number(), elasticsearchServiceAddress);
      return client;
    } catch (Exception e) {
      log.error("No cluster is running yet at ${elasticsearchServiceAddress}", e);
      return null;
    }
  }


  public ElasticsearchClient getClient() {

    if ( this.esclient == null )
      this.esclient = ensureClient(es_host_name)
    return esclient;
  }

  @javax.annotation.PreDestroy
  public void destroy() {
    log.debug("Destroy");
     if ( esclient ) {
       esclient.close();
     }
  }

  public void doDelete(String es_index, String record_id) {
    log.debug("doDelete(${es_index},${record_id})");
    // See https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/_motivations_around_a_new_java_client.html
    ElasticsearchClient esclient = getClient();


    DeleteRequest deleteRequest = new DeleteRequest.Builder()
      .index(index) // Replace with your index name
      .id(record_id)   // Replace with the ID of the document you want to delete
      .build();

    DeleteResponse response = esclient.delete(request);
  }

  public WrappedIndexResult doIndex(String es_index, 
                               String domain, 
                               String record_id, 
                               Map<String,Object> idx_record) {

    log.debug("doIndex(${es_index},${domain},${record_id},...)");

    ElasticsearchClient c = getClient()
    
    // Use the class level Object Mapper - it is thred safe (Apparently)
    JsonNode jsonNode = om.valueToTree(idx_record);

    // log.debug("json node is ${jsonNode}");

    IndexRequest<ObjectNode> request = IndexRequest.of(r -> r
      .index(es_index)
      .id (record_id)
      .document(jsonNode)
    );

    IndexResponse response = c.index(request);

    log.debug("index result: ${response.result()}");

    WrappedIndexResult wir = new WrappedIndexResult();

    return wir
  }

  public void bulkIndex(String es_index,
                   String domain,
                   String record_id,
                   List<Map> idx_records) {

    // https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/indexing-bulk.html
    // See also BulkRequest bulk = new BulkRequest()

    log.debug("doIndex(${es_index},${domain},${record_id},...)");
    ElasticsearchClient esclient = getClient();

    throw new RuntimeException("Not implemented");

    BulkRequest.Builder bulkRequestBuilder = new BulkRequest.Builder();

    idx_records.each { idx_record ->

	    bulkRequestBuilder.operations(
		    BulkOperation.of(op -> op
			    .index(IndexOperation.of(i -> i
				    .index(es_index)
					  .id(record_id)
						.document(idx_record)
	        ))
		    )
			);
    }

	  BulkRequest bulkRequest = bulkRequestBuilder.build();
    BulkResponse bulkResponse = client.bulk(bulk_request);
  }

  public WrappedSearchResult searchJson(String index,
                                        String qstr,
                                        Integer offset,
                                        Integer max,
                                        String sortBy,
                                        String sortOrder) {
		try {
      return searchJson(index, qstr, offset, max, sortBy, sortOrder, ObjectNode.class);
		}
		catch ( java.net.ConnectException e ) {
			log.error("Failing to connect to ES.. Reconnect")
			try {
				this.esclient.close();
			}
			finally {
				this.esclient = null;
			}
		}
  }

    public WrappedSearchResult searchJson(String index,
                 String qstr,
                 Integer offset,
                 Integer max,
                 String sortBy,
                 String sortOrder,
                 Class searchResultType) {

    log.debug("ESSearchService::search - ${index} ${qstr}")

    WrappedSearchResult wsr = null;

    ElasticsearchClient esclient = getClient()

    // String encodedString = Base64.getEncoder().encodeToString(qstr.getBytes());
    // Query wq = QueryBuilders.wrapper(w->w.query(encodedString));


    try {
			List<SortOptions> sortOptions = new ArrayList<>();

			if ( sortBy != null ) {
				SortOrder so = sortOrder?.equalsIgnoreCase('desc') ? SortOrder.Desc : SortOrder.Asc;
				sortOptions.add(SortOptions.of(sop -> sop
					.field( f -> f
						.field(sortBy)
						.order(so))));
			}

      SearchRequest searchRequest = SearchRequest.of( r -> r
              .trackTotalHits(h -> h.enabled(Boolean.TRUE))
              .size(max)
              .from(offset)
              .index(index)
              .sort( sortOptions )
              // .query(wq)
              .withJson(new StringReader(qstr))
      );

      // log.debug("Created search request ${searchRequest}");

      SearchResponse<ObjectNode> searchResponse = esclient.search(searchRequest, searchResultType);

      if ( searchResponse == null)
        throw new RuntimeException("ES Search returned null searching ${index} for ${qstr} order ${sortBy}");

      wsr = WrappedSearchResult.builder()
                .hits(searchResponse.hits().hits())
                .totalHits(searchResponse.hits().total().value())
                .build();
    }
    finally {
      log.debug("Search result(${index},${qstr}) = ${wsr?.getTotalHits()}");
    }

    return wsr;
  }


  def searchQS(String index, String query) {

    // log.debug("ESSearchService::search - ${index} ${query}")
    def result = [:]
    ElasticsearchClient esclient = getClient()
    try {

      SearchRequest searchRequest = new SearchRequest.Builder()
        .index(index)
        .query(Query.of(q -> q
          .queryString(qs -> qs
            .query(query)
          )
        ))
        .build();

       SearchResponse<Map> searchResponse = esclient.search(searchRequest, Map.class);
    }
    catch ( Exception e ) {
      log.error("Problem",e);
    }
    finally {
      log.debug("Search result = ${result.resultsTotal}");
    }

    result
  }

  def deleteByJsonQuery(String index, String query) {

    // https://artifacts.elastic.co/javadoc/org/elasticsearch/client/elasticsearch-rest-high-level-client/7.4.2/org/elasticsearch/client/RestHighLevelClient.html
    // see https://www.elastic.co/guide/en/elasticsearch/client/java-rest/master/java-rest-high-document-delete-by-query.html
    def result = [:]
    ElasticsearchClient esclient = getClient()

		if ( esclient == null )
      throw new RuntimeException("No client available");

    try {
      DeleteByQueryRequest deleteRequest = new DeleteByQueryRequest.Builder()
        .index(index)
        .query(Query.of(q -> q
          .queryString(qs -> qs
            .query(query)
          )
        ))
        .build();

				// Was
        // .query(q -> q
        //             .term(t -> t
        //                 .field("your-field-name") // Replace with the field name
        //                 .value(v -> v.stringValue("your-value")) // Replace with the value to delete by
        //            )
        //)

        // Perform the DeleteByQuery operation
        DeleteByQueryResponse response = esclient.deleteByQuery(deleteRequest);
    }
    catch ( Exception e ) {
      log.error("Problem in deleteByJsonQuery",e);
    }

    result


  }

  public static class WrappedIndexResult {
  }

  public void ensureIndex(String indexName, String indexTemplate) {

    log.debug("Validate ES index ${indexName}");

    ElasticsearchClient esclient = getClient();
    BooleanResponse result = esclient.indices().exists(co.elastic.clients.elasticsearch.indices.ExistsRequest.of(e -> e.index(indexName) ) );
    if ( result.value() ) {
      log.debug("${indexName} index exists");
    }
    else {
      log.info("${indexName} index does not exist - establish");
      if ( indexTemplate.equals('subscription') )
        createIndexUsingTemplate(indexName,'templates/esSubs.json');
      else if ( indexTemplate.equals('alert') )
        createIndexUsingTemplate(indexName, 'templates/esAlerts.json');
      else if ( indexTemplate.equals('eventcodes') )
        createIndexUsingTemplate(indexName, 'templates/esEventCodes.json');
      else
        throw new IllegalArgumentException("Unknown indexTemplate ${indexTemplate} when creating index ${indexName}".toString());
    }
  }

  // https://stackoverflow.com/questions/70856616/how-to-create-an-index-using-the-new-java-api-client-by-providing-source
  public void createIndexUsingTemplate(String indexName, String templateResource) {

    log.debug("createIndexUsingTemplate(${indexName}, ${templateResource})");

    String mappingText = new ClassPathResource(templateResource).getFile().text;
    ElasticsearchClient esclient = getClient();
    JsonpMapper mapper = esclient._transport().jsonpMapper();
    JsonParser parser = mapper.jsonProvider().createParser(new StringReader(mappingText));

    esclient.indices()
      .create(createIndexRequest -> createIndexRequest.index(indexName)
        .mappings(TypeMapping._DESERIALIZER.deserialize(parser, mapper)));
  }

}
