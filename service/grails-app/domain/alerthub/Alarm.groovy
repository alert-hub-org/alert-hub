package alerthub

import java.time.Instant;

class Alarm {

  // The "Code" by which this feed is known
	String id
  String resource
  String alarmCode
	Instant firstSeen;
	Instant lastSeen;
	Long repeat;

  static constraints = {
    id blank: false, nullable:false
    resource blank: false, nullable:false
    alarmCode blank: false, nullable:false
    firstSeen nullable:false
    lastSeen nullable:false
    repeat nullable:false
  }

  static mapping = {
    id generator:'assigned'
    version false
  }

 
}
