package alerthub

class Setting {

  String key
  String value

  static constraints = {
    key nullable:false, blank: false, unique: true
    value nullable:true, blank: true
  }

  static mapping = {
    table 'ah_setting'
  }

  public String toString() {
    "${key}:${value}"
  }
}
