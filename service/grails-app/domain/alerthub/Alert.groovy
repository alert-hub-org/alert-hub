package alerthub

class Alert {

    private static final long serialVersionUID = 1

    String id;
    String snowflake;
    String content;
    String identifier;
    String sender;
    String sent;
    String status;
    String msgType;
    String source;
    String restriction;
    String addresses;
    String code;
    String note;
    String references;
    String incidents;
    String scope;
    String rawAlert;
    String url;
    String analysisYearMonth;
    // Year-month-country-eventcode-certainty
    String analysisFull;


    static constraints = {
        id bindable: true
        snowflake blank:false, nullable: true;
        content blank:false, nullable: true
        identifier blank:false, nullable: true
        sender blank:false, nullable: true
        sent blank:false, nullable: true
        status blank:false, nullable: true
        msgType blank:false, nullable: true
        source blank:false, nullable: true
        restriction blank:false, nullable: true
        addresses blank:false, nullable: true
        code blank:false, nullable: true
        note blank:false, nullable: true
        references blank:false, nullable: true
        incidents blank:false, nullable: true
        scope blank:false, nullable: true
        rawAlert nullable:false
        url nullable:true
        analysisYearMonth nullable: true
        analysisFull nullable: true
    }

    static mapping = {
        table 'alert_sor'
        id column: 'al_id', generator: 'assigned', length:36
        snowflake column: 'al_snowflake_id', length:36, indexColumn:[name:'snowflake_id_idx']
        content column: 'al_content', type: 'text'
        identifier column:'al_identifier', length: 512
        sender column:'al_sender'
        sent column:'al_sent'
        status column:'al_status'
        msgType column:'al_msgType'
        source column:'al_source'
        restriction column:'al_restriction'
        addresses column:'al_addresses'
        code column:'al_code'
        note column:'al_note'
        references column:'al_references', length: 512
        incidents column:'al_incidents'
        scope column:'al_scope'
        rawAlert column: 'al_raw_alert', type:'text'
        url column:'al_url'
        analysisYearMonth column:'al_ym'
        analysisFull column:'al_full'
    }
}
