package feedfacade

class Tag {

  String tag

  static hasMany = [
  ]

  static mappedBy = [
  ]

  static constraints = {
    tag blank: false, nullable:false, unique: true
  }

  static mapping = {
    tag column: 'tag', index: 'tag_idx'
  }

}
