import alerthub.UserPasswordEncoderListener
import alerthub.services.pubsub.*
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.config.ClasspathYamlConfig;
import grails.util.Environment;
import alerthub.utils.Snowflake;

//  https://docs.grails.org/6.2.0/guide/spring.html
// Place your Spring DSL code here
beans = {
  snowflake(Snowflake);
  userPasswordEncoderListener(UserPasswordEncoderListener);
  alertHubEventPublisher(GrailsInternalEventPublisher);
  // https://docs.hazelcast.com/tutorials/hazelcast-embedded-springboot

  config(ClasspathYamlConfig, ( System.getenv('KUBERNETES_SERVICE_HOST') == 'hazelcast-k8s.yaml' ? '' : 'hazelcast.yaml') );

  // Using the constructor arg ref('config') passes a parameter to the factoryMethod
  hazelcastInstance(Hazelcast, ref('config')) { bean ->
    bean.factoryMethod='newHazelcastInstance'
    bean.scope='singleton'
  }

  switch(Environment.current) {
    case Environment.TEST:
      // Switch test beans out here
      break;
    default:
      // otherwise prod beans
      break;
  }
}

