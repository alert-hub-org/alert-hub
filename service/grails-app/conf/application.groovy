

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'alerthub.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'alerthub.UserRole'
grails.plugin.springsecurity.authority.className = 'alerthub.Role'
grails.plugin.springsecurity.rest.token.storage.jwt.secret = System.getenv('KCSECRET')?:'nonenonenonenonenonenonenonenone';

// Remember that these patterns are based on the controller/action not the URL Mappings
// So use /home/topic instead of /topics/**
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',                  access: ['permitAll']],
  [pattern: '/actuator/**',       access: ['ROLE_ADMIN', 'isFullyAuthenticated()']],
	[pattern: '/sample/**',         access: ['permitAll']],
	[pattern: '/setup/**',          access: ['permitAll']],
	[pattern: '/logout',            access: ['permitAll']],
	[pattern: '/error',             access: ['permitAll']],
	[pattern: '/subscriptions',     access: ['permitAll']],
	[pattern: '/subscriptions/**',  access: ['permitAll']],
	[pattern: '/alert',             access: ['permitAll']],
	[pattern: '/alert/**',          access: ['permitAll']],
	[pattern: '/api/**',            access: ['permitAll']],
	[pattern: '/home',              access: ['permitAll']],
	[pattern: '/home/index',        access: ['permitAll']],
	[pattern: '/home/topic',        access: ['permitAll']],
	[pattern: '/home/about',        access: ['permitAll']],
	[pattern: '/home/explorer1',    access: ['permitAll']],
	[pattern: '/home/info',         access: ['permitAll']],
  [pattern: '/hubClient/**',      access: ['permitAll']],
	[pattern: '/index.gsp',         access: ['permitAll']],
	[pattern: '/shutdown',          access: ['permitAll']],
	[pattern: '/assets/**',         access: ['permitAll']],
	[pattern: '/**/js/**',          access: ['permitAll']],
	[pattern: '/**/css/**',         access: ['permitAll']],
	[pattern: '/**/images/**',      access: ['permitAll']],
	[pattern: '/**/favicon.ico',    access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'],
  // Stateless filter chain
	[pattern: '/api/**',         filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter']
]


println('./grails-app/conf/application.groovy '+ System.currentTimeMillis());
println("\n\nAH_ES_HOST:: ${System.getenv('AH_ES_HOST')}\n\n");

/*
elasticSearch = [
  client:[
    mode: 'transport',
    hosts:[
      [ 
        host: System.getenv('AH_ES_HOST')?:'elasticsearch',
        port: 9300
      ]
    ]
  ]
]
println("ES config block: ${elasticSearch}");
*/

eshost = System.getenv('AH_ES_HOST')?:'http://localhost:9200';

