package alerthub.utils;

import net.sf.json.JSONNull;

import java.util.Map;
import java.util.Iterator;

public class Utils {

  public static String XmlToJson(String xml_document) {
    net.sf.json.xml.XMLSerializer xs = new net.sf.json.xml.XMLSerializer();
    xs.setSkipNamespaces(true);
    // xs.setSkipWhitespace(true);
    xs.setTrimSpaces(true);
    xs.setNamespaceLenient(true);
    xs.setRemoveNamespacePrefixFromElements(true);

    net.sf.json.JSON json_obj = xs.read(xml_document);
    String json_text = json_obj.toString(2);

    return json_text;
  }

  public static Map<String,Object> XmlToMap(String xml_document) {
    return XmlToMap(xml_document, true);
  }

    public static Map<String,Object> XmlToMap(String xml_document, boolean prune_nulls) {
    net.sf.json.xml.XMLSerializer xs = new net.sf.json.xml.XMLSerializer();
    xs.setSkipNamespaces(true);
    // xs.setSkipWhitespace(true);
    xs.setTrimSpaces(true);
    xs.setNamespaceLenient(true);
    xs.setRemoveNamespacePrefixFromElements(true);

    net.sf.json.JSON json_obj = xs.read(xml_document);

    Map<String,Object> result = (Map<String,Object>) json_obj;

    if ( prune_nulls )
      prune(result);

    return result;
  }


  public static void prune(Map<String, Object> map) {
    if (map == null) {
      return;
    }

    Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, Object> entry = iterator.next();
      if ( ( entry.getValue() == null) || ( entry.getValue() instanceof JSONNull ) ) {
        iterator.remove();
      } else if (entry.getValue() instanceof Map) {
        prune((Map<String, Object>) entry.getValue());
        // After recursion, check if the nested map is empty
        if (((Map<?, ?>) entry.getValue()).isEmpty()) {
          iterator.remove();
        }
      }
    }
  }
}
