package alerthub.utils;

public class DuplicateAlertException extends Exception {

    public DuplicateAlertException(){
      super();
    }

    public DuplicateAlertException(String message){
        super(message);
    }
}
