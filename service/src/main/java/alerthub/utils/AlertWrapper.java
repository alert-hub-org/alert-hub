package alerthub.utils;

import alerthub.cap.Alert;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Map;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class AlertWrapper {

    public byte[] rawCAP;
    public Alert pojoCAP;
    public Map<String,Object> alertMetadata;
    public Map<String,Object> alertBody;
		public List<String> alarms;
    public String evtTimestampStr;
    public String url;
		public List<String> processingFlags;
}

