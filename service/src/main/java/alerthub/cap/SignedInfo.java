package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class SignedInfo {
    @JacksonXmlProperty(localName = "CanonicalizationMethod")
    private CanonicalizationMethod canonicalizationMethod;
    @JacksonXmlProperty(localName = "SignatureMethod")
    private SignatureMethod signatureMethod;
    @JacksonXmlProperty(localName = "Reference")
    private Reference reference;
}
