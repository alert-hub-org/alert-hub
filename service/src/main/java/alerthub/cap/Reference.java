package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;
import java.util.List;
import lombok.*;

@Data
public class Reference {


    @JacksonXmlProperty(localName = "URI", isAttribute = true)
    private String uri;

    @JacksonXmlProperty(localName = "Transform")
    @JacksonXmlElementWrapper(localName = "Transforms")
    private List<Transform> transforms;

    @JacksonXmlProperty(localName = "DigestMethod")
    private DigestMethod digestMethod;

    @JacksonXmlProperty(localName = "DigestValue")
    private String digestValue;
}
