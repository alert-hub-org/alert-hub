package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
public class CanonicalizationMethod {

    @JacksonXmlProperty(isAttribute = true, localName = "Algorithm")
    private String algorithm;
}
