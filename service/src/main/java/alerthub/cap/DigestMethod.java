package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class DigestMethod {
    @JacksonXmlProperty(isAttribute = true, localName = "Algorithm")
    private String algorithm;
}
