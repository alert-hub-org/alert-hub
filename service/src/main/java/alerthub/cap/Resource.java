package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class Resource {


    @JacksonXmlProperty(localName = "resourceDesc")
    private String resourceDesc;

    @JacksonXmlProperty(localName = "mimeType")
    private String mimeType;

    @JacksonXmlProperty(localName = "uri")
    private String uri;

    @JacksonXmlProperty(localName = "size")
    private String size;
}
