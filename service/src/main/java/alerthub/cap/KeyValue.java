package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class KeyValue {

    @JacksonXmlProperty(localName = "RSAKeyValue")
    private RSAKeyValue rsaKeyValue;

}
