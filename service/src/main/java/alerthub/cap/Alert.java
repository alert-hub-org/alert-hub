package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JacksonXmlRootElement(localName = "alert", namespace="urn:oasis:names:tc:emergency:cap:1.2")
public class Alert {

    @JacksonXmlProperty(localName = "identifier")
    private String identifier;

    @JacksonXmlProperty(localName = "sender")
    private String sender;

    @JacksonXmlProperty(localName = "sent")
    private String sent;

    @JacksonXmlProperty(localName = "status")
    private String status;

    @JacksonXmlProperty(localName = "msgType")
    private String msgType;

    @JacksonXmlProperty(localName = "source")
    private String source;

    @JacksonXmlProperty(localName = "restriction")
    private String restriction;

    @JacksonXmlProperty(localName = "addresses")
    private String addresses;

    @JacksonXmlProperty(localName = "code")
    private String code;

    @JacksonXmlProperty(localName = "note")
    private String note;

    @JacksonXmlProperty(localName = "references")
    private String references;

    @JacksonXmlProperty(localName = "incidents")
    private String incidents;

    @JacksonXmlProperty(localName = "scope")
    private String scope;

    @JacksonXmlProperty(localName = "info")
    private List<Info> info;

    @JacksonXmlProperty(localName = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
    private Signature signature;

}
