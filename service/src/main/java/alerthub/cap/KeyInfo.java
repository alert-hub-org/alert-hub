package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class KeyInfo {

    @JacksonXmlProperty(localName = "X509Data")
    private X509Data x509Data;

    @JacksonXmlProperty(localName = "KeyValue")
    private KeyValue keyValue;

}
