package alerthub.cap;

import java.util.List;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class Info {

    @JacksonXmlProperty(localName = "language")
    private String language;

    @JacksonXmlProperty(localName = "category")
    private String category;

    @JacksonXmlProperty(localName = "event")
    private String event;

    @JacksonXmlProperty(localName = "responseType")
    private String responseType;

    @JacksonXmlProperty(localName = "urgency")
    private String urgency;

    @JacksonXmlProperty(localName = "severity")
    private String severity;

    @JacksonXmlProperty(localName = "certainty")
    private String certainty;

    @JacksonXmlProperty(localName = "audience")
    private String audience;

    @JacksonXmlProperty(localName = "eventCode")
    private String eventCode;

    @JacksonXmlProperty(localName = "effective")
    private String effective;

    @JacksonXmlProperty(localName = "onset")
    private String onset;

    @JacksonXmlProperty(localName = "expires")
    private String expires;

    @JacksonXmlProperty(localName = "senderName")
    private String senderName;

    @JacksonXmlProperty(localName = "headline")
    private String headline;

    @JacksonXmlProperty(localName = "description")
    private String description;

    @JacksonXmlProperty(localName = "instruction")
    private String instruction;

    @JacksonXmlProperty(localName = "web")
    private String web;

    @JacksonXmlProperty(localName = "contact")
    private String contact;

    @JacksonXmlProperty(localName = "parameter")
    private List<Parameter> parameter;

    @JacksonXmlProperty(localName = "resource")
    private List<Resource> resource;

    @JacksonXmlProperty(localName = "area")
    private List<Area> area;
}
