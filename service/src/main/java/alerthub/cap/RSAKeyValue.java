package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class RSAKeyValue {

    @JacksonXmlProperty(localName = "Modulus")
    private String modulus;

    @JacksonXmlProperty(localName = "Exponent")
    private String exponent;

}
