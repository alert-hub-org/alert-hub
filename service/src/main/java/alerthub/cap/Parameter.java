package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class Parameter {

    @JacksonXmlProperty(localName = "valueName")
    private String valueName;

    @JacksonXmlProperty(localName = "value")
    private String value;
}
