package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.*;

@Data
public class X509Data {
    @JacksonXmlProperty(localName = "X509Certificate")
    private String x509Certificate;
}
