package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class SignatureProperty {

    @JacksonXmlProperty(localName = "Id", isAttribute = true)
    private String id;
    @JacksonXmlProperty(localName = "Target", isAttribute = true)
    private String target;
    @JacksonXmlProperty(localName = "value", namespace = "http://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2.xsd")
    private String value;
}
