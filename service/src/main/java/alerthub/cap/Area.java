package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class Area {

    @JacksonXmlProperty(localName = "areaDesc")
    private String areaDesc;

    @JacksonXmlProperty(localName = "polygon")
    private String polygon;

    @JacksonXmlProperty(localName = "circle")
    private String circle;

    @JacksonXmlProperty(localName = "geocode")
    private String geocode;

    @JacksonXmlProperty(localName = "altitude")
    private String altitude;

    @JacksonXmlProperty(localName = "ceiling")
    private String celing;
}
