package alerthub.cap;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.*;

@Data
public class Signature {

    @JacksonXmlProperty(localName = "Id", isAttribute = true)
    private String id;
    @JacksonXmlProperty(localName = "SignedInfo")
    private SignedInfo signedInfo;
    @JacksonXmlProperty(localName = "SignatureValue")
    private String signatureValue;
    @JacksonXmlProperty(localName = "KeyInfo")
    private KeyInfo keyInfo;
    @JacksonXmlProperty(localName = "Object")
    private SigObject sigObject;
}
