package alerthub.cap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class Transform {
    @JacksonXmlProperty(isAttribute = true, localName = "Algorithm")
    private String algorithm;
}
