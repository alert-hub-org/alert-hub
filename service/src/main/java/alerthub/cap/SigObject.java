package alerthub.cap;

import java.util.List;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.*;

@Data
public class SigObject {

    @JacksonXmlProperty(localName = "SignatureProperty")
    @JacksonXmlElementWrapper(localName = "SignatureProperties")
    private List<SignatureProperty> signatureProperties;
}
