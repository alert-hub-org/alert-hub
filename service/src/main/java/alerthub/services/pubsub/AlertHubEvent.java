package alerthub.services.pubsub;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.Map;

@Data
@AllArgsConstructor
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class AlertHubEvent {

  @ToString.Include
  public String eventCode;

  @ToString.Include
  public String routingKey;

  @ToString.Include
  public String exchange;

  public Object body;

  public Map<String,Object> headers;
}
