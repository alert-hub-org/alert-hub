package alerthub.services.pubsub;

/*
  CapEventHandlerService.groovy:  @Subscriber('alerthub:CapEvent')
  AtomEventHandlerService.groovy:  @Subscriber('alerthub:AtomEvent')
  StaticFeedService.groovy:  @Subscriber 
  RssEventHandlerService.groovy:  @Subscriber('AlertHub:RSSEvent')
*/
public interface AlertHubEventPublisher {


  public void publish(AlertHubEvent evt);

}
