package alerthub.services.pubsub;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HazelcastEventPublisher implements AlertHubEventPublisher {

  @javax.annotation.PostConstruct
  public void init() {
    log.info("HazelcastEventPublisher::init");
  }

  public void publish(AlertHubEvent evt) {

    log.info("HazelcastEventPublisher::publish({})", evt);
  }

}
