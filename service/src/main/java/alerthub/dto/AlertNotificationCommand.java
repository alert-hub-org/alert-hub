package alerthub.dto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AlertNotificationCommand {
	private List<AlertNotificationDetail> alerts;
	private String sourceSystem;
	private String signature;
	private Boolean forceProcessing;
	
}
