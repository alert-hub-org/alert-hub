package alerthub.dto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PreflightCommand {
  private List<String> identifiers;
}

