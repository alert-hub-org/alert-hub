package alerthub.dto;

import lombok.*;
import java.util.List;

@lombok.Data
@lombok.Builder
@lombok.NoArgsConstructor
@lombok.AllArgsConstructor
public class WrappedSearchResult {
    private List<Object> hits;
    private Long totalHits;

}