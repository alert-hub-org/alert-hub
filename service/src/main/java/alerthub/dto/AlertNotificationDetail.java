package alerthub.dto;


import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AlertNotificationDetail {
    String source;
    String url;
}
