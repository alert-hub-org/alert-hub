package alerthub.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class NotifyResult {
    private String resultCode;
}
