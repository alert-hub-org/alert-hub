package alerthub.services.pubsub;

import grails.events.EventPublisher;
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@Slf4j
public class GrailsInternalEventPublisher implements AlertHubEventPublisher, EventPublisher {

  @javax.annotation.PostConstruct
  public void init() {
    log.info("GrailsInternalEventPublisher::init");
  }

  public void publish(AlertHubEvent evt) {
    if ( ( evt != null ) && ( evt.getEventCode() != null ) ) {
      log.debug("GrailsInternalEventPublisher::publish({},{})", evt.getEventCode(), evt);
      notify(evt.getEventCode(), evt);
    }
    else {
      log.error("PUBLISH CALLED WITH NULL EVENT or EVENT CODE evt:${evt}");
      throw new RuntimeException("PUBLISH CALLED WITH NULL EVENT or EVENT CODE evt-present:${evt!=null}")
    }
  }

}
