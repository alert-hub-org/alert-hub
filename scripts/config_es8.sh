export ESHOST="localhost"
export ESUSER="elastic"
export ESPASS="elastic"

echo Update index referesh interval to 15s

curl -u $ESUSER:$ESPASS -XPUT 'http://localhost:9200/_all/_settings?preserve_existing=true' -H 'Content-Type: application/json' -d '{
"index.refresh_interval" : "15s"
}'
echo

echo Clear down events
curl -s -u $ESUSER:$ESPASS -XDELETE "http://$ESHOST:9200/events"
echo
echo Create events
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/events"
echo

echo Clear down alerts
# Clear down
curl -s -u $ESUSER:$ESPASS -XDELETE "http://$ESHOST:9200/alerts"
echo
echo Create Alerts
# Create an index called alerts
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alerts"
echo
echo
# Create a type mapping called alert
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alerts/_mapping" -H 'Content-Type: application/json' -d ' 
{ 
      "date_detection": false,
      "properties":{ 
         "id":{ 
            "index":true,
            "type":"keyword", 
            "store":true
         }, 
         "AlertMetadata":{
           "properties":{
             "MatchedSubscriptions":{
               "type":"keyword",
               "index":true
             },
             "Expires":{
               "type":"date"
             },
             "Effective":{
               "type":"date"
             }
           }
         },
         "AlertBody":{
           "properties":{
             "info":{
               "properties":{
                 "parameter":{
                   "properties":{
                     "value":{
                       "type":"text"
                     }
                   }
                 },
                 "area":{
                   "properties":{
                     "cc_polys" : {
                       "type": "geo_shape"
                     }
                   }
                 },
                 "effective":{
                   "type":"date"
                 },
                 "expires":{
                   "type":"date"
                 }
               }
             },
             "identifier": {
               "type":"keyword",
               "index":true
             }
           }
         },
         "evtTimestamp":{
           "type":"date"
         }
      }
}' 


echo Clear down alertarchive
# Clear down
curl -s -u $ESUSER:$ESPASS -XDELETE "http://$ESHOST:9200/alertarchive"
echo Create alertarchive
# Create an index called alertarchive
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alertarchive"
echo
# Create a type mapping called alertarchive
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alertarchive/_mapping" -H 'Content-Type: application/json' -d ' 
{ 
      "date_detection": false,
      "properties":{ 
         "id":{ 
            "index":true,
            "type":"keyword", 
            "store":true
         }, 
         "AlertMetadata":{
           "properties":{
             "MatchedSubscriptions":{
               "type":"keyword",
               "index":true
             },
             "Expires":{
               "type":"date"
             },
             "Effective":{
               "type":"date"
             }
           }
         },
         "AlertBody":{
           "properties":{
             "info":{
               "properties":{
                 "parameter":{
                   "properties":{
                     "value":{
                       "type":"text"
                     }
                   }
                 },
                 "area":{
                   "properties":{
                     "cc_polys" : {
                       "type": "geo_shape"
                     }
                   }
                 },
                 "effective":{
                   "type":"date"
                 },
                 "expires":{
                   "type":"date"
                 }
               }
             },
             "identifier": {
               "type":"keyword",
               "index":true
             }
           }
         },
         "evtTimestamp":{
           "type":"date"
         }
      }
}' 
echo


echo Clear down subscriptions
curl -s -u $ESUSER:$ESPASS -XDELETE "http://$ESHOST:9200/alertssubscriptions"
echo
echo Create subscriptions
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alertssubscriptions"
echo
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/alertssubscriptions/_mapping" -H 'Content-Type: application/json' -d ' 
{ 
      "properties":{ 
         "id":{ 
            "index":true,
            "type":"keyword", 
            "store":true
         }, 
         "subshape": {
            "type": "geo_shape"
         }
      }
}'
echo

echo Clear down gaz
curl -s -u $ESUSER:$ESPASS -XDELETE "http://$ESHOST:9200/gazetteer"
echo
echo Create gaz
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/gazetteer"
curl  -u $ESUSER:$ESPASS -XPUT "http://$ESHOST:9200/gazetteer/_mapping" -H 'Content-Type: application/json' -d ' 
{ 
      "properties":{ 
         "id":{ 
            "index":true,
            "type":"keyword", 
            "store":true
         }, 
         "subshape": {
            "type": "geo_shape"
         }
      }
}'
echo
echo CAP ES Setup script completed
