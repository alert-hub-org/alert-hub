#!/usr/bin/env groovy

import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('io.github.http-builder-ng:http-builder-ng-apache:1.0.4')

import org.ini4j.*;
import java.nio.file.*;
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import javax.xml.bind.DatatypeConverter
import java.nio.file.attribute.BasicFileAttributes;
import groovy.transform.Field

// Moving to Apache http client implementation for HttpBuilderNG
import groovyx.net.http.ApacheHttpBuilder
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.client.config.RequestConfig
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer

def cli = new CliBuilder(usage: 'sign.groovy')

// Create the list of options.
cli.with {
  h longOpt: 'help', 'Show usage information'
  c longOpt: 'config', args: 1, argName: 'config', 'config name'
  s longOpt: 'sourcedir', args: 1, argName: 'sourcedir', 'Source directory'
}

def options = cli.parse(args)

if (!options ) {
  System.err.println("No config or no definitions file");
  System.err.println("usage: groovy sign.groovy definitions_file.json");
  return
}

println("Starting....");

@Field
static List<String> pending_alert_filenames = [];

String cfgname=options.c ?: 'default'

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.config/ah-signing2'));

String private_key_file = ini.get(cfgname, 'private_key', String.class);
String public_key_file = ini.get(cfgname, 'public_key', String.class);
String target_alert_hub = ini.get(cfgname, 'target_system', String.class);
String cfg_source_dir = ini.get(cfgname, 'sourcedir', String.class);

String sourcedir=options.s ?: cfg_source_dir

String alert_base_url = 'https://downloads.alert-hub.org/cap-alerts/';

@Field
static int MAX_HTTP_TIME = 25 * 1000;

HttpBuilder http_client = ApacheHttpBuilder.configure {
  request.uri = target_alert_hub
  client.clientCustomizer { HttpClientBuilder builder ->
    RequestConfig.Builder requestBuilder = RequestConfig.custom()
    requestBuilder.connectTimeout = MAX_HTTP_TIME;
    requestBuilder.connectionRequestTimeout = MAX_HTTP_TIME;
    requestBuilder.socketTimeout = MAX_HTTP_TIME;
    builder.defaultRequestConfig = requestBuilder.build()
  }
}

// println "sign.groovy"
if ( private_key_file == null ) {
  println("Invalid configuration - missing private key file");
  System.exit(1);
}


PublicKey pub_key = getPublicKey(new File(public_key_file));
PrivateKey priv_key = getPrivateKey(new File(private_key_file));




/*
Map<String,Object> request = [
  urls:[ args[0] ],
  signature: null,
  sourceSystem: "AHPrime"
]

String urls_to_sign = request.urls.toString();
byte[] request_signed_bytes = urls_to_sign.getBytes();
byte[] signature = getSignature(request_signed_bytes, priv_key);
String encoded_signature = Base64.encodeBase64String(signature);

*/

// Recurse from this dir down looking for alerts;
Map<String,Object> ingest_context = [
  'pagecount': Long.valueOf(1)
];

findAlerts(sourcedir, sourcedir, http_client, ingest_context);

// Mop any any left over alerts from the context that should be sent
sendBatch(http_client, ingest_context);

/*
request.signature = encoded_signature;

// byte[] decoded_sig = Base64.decodeBase64(encoded_sig)

println("sig input: ${request_signed_bytes}");
println("Signature: ${encoded_signature}");
println("Target URL: ${target_alert_hub}/api/vi/notifyAlert");

println("Payload: ${new JsonBuilder(request).toPrettyString()}");
*/

System.exit(0);


public static findAlerts(String directory, String base, HttpBuilder http_client, Map<String, Object> context) {

    def dirPath = Paths.get(directory)
    String dirName = dirPath.getName(dirPath.getNameCount()-1).toString();
    int counter = 0;

    if (!Files.exists(dirPath) || !Files.isDirectory(dirPath)) {
        println "Directory does not exist: ${directory}"
        return
    }

    Files.walkFileTree(dirPath, new SimpleFileVisitor<Path>() {
        @Override
        FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            if ( (file.toString().endsWith('.xml')) && ( !file.getFileName().toString().equals('rss.xml') ) ) {
                String subpath_to_alert = file.toString().replace(base,'/'+dirName);
		counter++;
                notifyAlert(subpath_to_alert, http_client, context)
            }
            return FileVisitResult.CONTINUE
        }

        @Override
        FileVisitResult visitFileFailed(Path file, IOException exc) {
            println "Failed to access file: ${file} due to ${exc.message}"
            return FileVisitResult.CONTINUE
        }
    })

    println("***SUMMARY*** ${directory} ${counter}");
}

public static notifyAlert(String alert_file, HttpBuilder http_client, Map<String,Object>context) {

  this.pending_alert_filenames.add('https://downloads.alert-hub.org/cap-alerts'+alert_file);
  if ( this.pending_alert_filenames.size() >= 10 ) {
    println("Notify ${pending_alert_filenames}");
    sendBatch(http_client, context);
  }

}

public static sendBatch(HttpBuilder http_client, Map<String,Object>context) {
  // println("Sending batch ${this.pending_alert_filenames} to client path /api/v1/notifyAlert}");
  println("Sending page [${context.pagecount}]  of ${this.pending_alert_filenames?.size()} to client path /api/v1/notifyAlert}");

  if ( ( pending_alert_filenames == null ) || ( pending_alert_filenames?.size() == 0 ) )
    return;

  println("first entry is ${pending_alert_filenames.get(0)}");

  List<Map<String, Object>> alerts = [];
  pending_alert_filenames.each { paf ->

    // https://downloads.alert-hub.org/cap-alerts/lv-lvgmc-lv/d
    def pattern = /.*cap-alerts\/([^\/]+)\/.*/
    def matcher = (paf =~ pattern)

    String inferred_source = null;
    if (matcher) {
      inferred_source = matcher[0][1]
    }

    alerts.add([source:inferred_source, url:paf]);
  }

  Map<String,Object> req = [
    alerts: alerts,
    signature: "sigsigsig",
    sourceSystem: "AHPrime"
  ]

  int retry_count = 0;
  boolean success = false;

  while ( !success && ( retry_count < 10 ) ) {

    println("${new Date()} - Attempting post");

    try {
      http_client.post {
        request.uri.path='/CAPAggregator/api/v1/notifyAlert'
        request.body = req
        request.contentType = 'application/json'
        request.headers['accept'] = 'application/json'

        response.success { FromServer resp ->
          println("SUCCESS")
          pending_alert_filenames.clear();
          context.pagecount = Long.valueOf(context.pagecount+1);
          success=true;
          println("Attempt [${retry_count}] : OK");
        }

        response.failure { FromServer resp ->
          println("FAILURE code=${resp.getStatusCode()}");
          println("FAILURE message=${resp.getMessage()}");
          println(resp.reader?.text);
          retry_count++;
          println("Attempt [${retry_count}] : FAIL");
          Thread.sleep(10000);
        }
      }
      Thread.sleep(1000);
    }
    catch ( Exception e ) {
      println("Exception in post ${e.message}... sleeping");
      Thread.sleep(10000);
    }
  }
}

public static RSAPublicKey getPublicKey(File file) throws Exception {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String publicKeyPEM = key
      .replace("-----BEGIN PUBLIC KEY-----", "")
      .replaceAll(System.lineSeparator(), "")
      .replace("-----END PUBLIC KEY-----", "");

    byte[] encoded = Base64.decodeBase64(publicKeyPEM);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    return (RSAPublicKey) keyFactory.generatePublic(keySpec);
}

// N.B this only works to load a pcks8 encoded file and an openssl generated private key is not in that formay
// and must be converted with
//  openssl pkcs8 -topk8 -nocrypt -in myprivate.pem -out myprivate.pcks8
public RSAPrivateKey getPrivateKey(File file) throws Exception {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String privateKeyPEM = key
      .replace("-----BEGIN PRIVATE KEY-----", "")
      .replaceAll(System.lineSeparator(), "")
      .replace("-----END PRIVATE KEY-----", "");

    byte[] encoded = Base64.decodeBase64(privateKeyPEM);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
}

public static byte[] getSignature(byte[] bytes, PrivateKey priv_key) {
  Signature sign = Signature.getInstance("SHA1withRSA");
  sign.initSign(priv_key);
  sign.update(bytes);
  return sign.sign();
}

public boolean verifySignature(byte[] bytes, byte[] sig, PublicKey pub_key) {
  Signature sig_inst = Signature.getInstance( "SHA1withRSA" );
  sig_inst.initVerify( pub_key );
  sig_inst.update( bytes );
  ret = sig_inst.verify( sig );
}

public static String getMD5(byte[] bytes) {
  MessageDigest md = MessageDigest.getInstance("MD5");
  md.update(bytes);
  byte[] digest = md.digest();
  return DatatypeConverter.printHexBinary(digest).toUpperCase();
}

