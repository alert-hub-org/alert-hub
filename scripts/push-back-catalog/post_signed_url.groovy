#!/usr/bin/env groovy

import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')

import org.ini4j.*;
import java.nio.file.*;
import java.security.*;
import java.security.spec.*;
import java.security.interfaces.*;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import javax.xml.bind.DatatypeConverter

def cli = new CliBuilder(usage: 'sign.groovy')

// Create the list of options.
cli.with {
  h longOpt: 'help', 'Show usage information'
  c longOpt: 'config', args: 1, argName: 'config', 'config name'
}

def options = cli.parse(args)

String definitions_file = options.arguments()[0]

if (!options || !definitions_file ) {
  System.err.println("No config or no definitions file");
  System.err.println("usage: groovy sign.groovy definitions_file.json");
  return
}

String cfgname=options.c ?: 'default'

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.config/ah-signing'));

String private_key_file = ini.get(cfgname, 'private_key', String.class);
String public_key_file = ini.get(cfgname, 'public_key', String.class);
String target_alert_hub = ini.get(cfgname, 'target_system', String.class);

// println "sign.groovy"
if ( private_key_file == null ) {
  println("Invalid configuration - missing private key file");
  System.exit(1);
}


PublicKey pub_key = getPublicKey(new File(public_key_file));
PrivateKey priv_key = getPrivateKey(new File(private_key_file));
Map<String,Object> request = [
  urls:[ args[0] ],
  signature: null,
  sourceSystem: "AHPrime"
]

String urls_to_sign = request.urls.toString();
byte[] request_signed_bytes = urls_to_sign.getBytes();
byte[] signature = getSignature(request_signed_bytes, priv_key);
String encoded_signature = Base64.encodeBase64String(signature);


request.signature = encoded_signature;

// byte[] decoded_sig = Base64.decodeBase64(encoded_sig)

println("sig input: ${request_signed_bytes}");
println("Signature: ${encoded_signature}");
println("Target URL: ${target_alert_hub}/api/vi/notifyAlert");

println("Payload: ${new JsonBuilder(request).toPrettyString()}");

System.exit(0);

public static RSAPublicKey getPublicKey(File file) throws Exception {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String publicKeyPEM = key
      .replace("-----BEGIN PUBLIC KEY-----", "")
      .replaceAll(System.lineSeparator(), "")
      .replace("-----END PUBLIC KEY-----", "");

    byte[] encoded = Base64.decodeBase64(publicKeyPEM);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
    return (RSAPublicKey) keyFactory.generatePublic(keySpec);
}

// N.B this only works to load a pcks8 encoded file and an openssl generated private key is not in that formay
// and must be converted with
//  openssl pkcs8 -topk8 -nocrypt -in myprivate.pem -out myprivate.pcks8
public RSAPrivateKey getPrivateKey(File file) throws Exception {
    String key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());

    String privateKeyPEM = key
      .replace("-----BEGIN PRIVATE KEY-----", "")
      .replaceAll(System.lineSeparator(), "")
      .replace("-----END PRIVATE KEY-----", "");

    byte[] encoded = Base64.decodeBase64(privateKeyPEM);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
    return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
}

public static byte[] getSignature(byte[] bytes, PrivateKey priv_key) {
  Signature sign = Signature.getInstance("SHA1withRSA");
  sign.initSign(priv_key);
  sign.update(bytes);
  return sign.sign();
}

public boolean verifySignature(byte[] bytes, byte[] sig, PublicKey pub_key) {
  Signature sig_inst = Signature.getInstance( "SHA1withRSA" );
  sig_inst.initVerify( pub_key );
  sig_inst.update( bytes );
  ret = sig_inst.verify( sig );
}

public static String getMD5(byte[] bytes) {
  MessageDigest md = MessageDigest.getInstance("MD5");
  md.update(bytes);
  byte[] digest = md.digest();
  return DatatypeConverter.printHexBinary(digest).toUpperCase();
}

