#!/bin/bash

echo hello


# Define the maximum number of concurrent jobs
MAX_JOBS=1

# Function to wait for a job slot to be free
wait_for_slot() {
    # Loop until there are fewer than MAX_JOBS background jobs running
    while [ $(jobs -rp | wc -l) -ge $MAX_JOBS ]; do
        # Wait for any background job to finish before checking again
        wait -n
    done
}

for DIR in `ls /home/ubuntu/data/cap-alerts` 
do
  wait_for_slot
  echo /home/ubuntu/data/cap-alerts/$DIR
  ./post_alert_archive.groovy -s "/home/ubuntu/data/cap-alerts/$DIR" > $DIR.log &
done
