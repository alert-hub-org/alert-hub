# curl -X GET 'http://localhost:9200/alertssubscriptions/_search' -d '{
curl -X GET 'https://betaes.alert-hub.hosting.semweb.co/alertssubsv1/_search' -H 'content-type: application/json' -d '{
     "from":0,
     "size":1000,
     "query":{
         "bool": {
           "must": {
             "match_all": {}
           },
           "filter": {
               "geo_shape": {
                 "subshape": {
                   "shape": {
                     "type":"circle",
                     "coordinates":[-70.06,12.58],
                     "radius": "20.8km"
                   },
                   "relation":"intersects"
                 }
               }
             }
           }
     }
}
'
