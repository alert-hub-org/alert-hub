latest changes for Alert Hub CAP Aggregator - docker images can be found at https://hub.docker.com/r/semweb/caphub_aggregator/tags. The head of master is continuously deployed to https://test.semweb.co/CAPAggregator/subscriptions/details/unfiltered?tag=mappable

2021-03-13 Open 2.1.19-SNAPSHOT

2021-03-13 Release 2.1.18

  * Migrate to HikariCP and Undertow servlet engine


