# About

AlertHub



# Development

Start postgres and elasticsearch in the scripts directory using "docker compose up"

Create the ES schemas by running scripts / config_es7.sh

Start the system from the service directory with ./gradlew bootRun






Alert event flows


See grails-app/conf/spring/resources.groovy to find out which event publisher is configured - most likely GrailsInternalEventPublisher


- Feed checker service is invoked periodically, it uses a combination of database locks and timers to try and pull the next feed needing a check off the queue - locks ensure only one instance can process a feed at a time
- If the FeedChecker detects a new entry, newEventService.handleNewEvent(id,entry) will fire


If the event is a new entry on an ATOM Feed, 



New Sub Flows

New subscriptions come in via capcollator/SubsImportService.groovy 
An AlertHubEvent is created for each new or updated sub



As feedCheckerService detects new feed entries, it calls newEventService.handleNewEvent for each new entry

newEventService will unpick whats going on and if it finds a valid CAP URL it will use alertHubEventPublisher.publish to emit an alerthub.RSS or alerthub.ATOM event

AtomEventHandlerService and RSSEventHandlerService look out for those events, and if a valid CAP URL is detected it will call 
     capUrlHandlerService.handleNotification(body.link, headers)


CapURLHandlerService is responsible for newly  detected Alert URLs - from feeds or even when enumerating the full alert archive from AWS S3 CAPUrlHandlerService
will call alertHubEventPublisher.broadcastCapEvent - and if all is well ultimately

      alertHubEventPublisher.publish(
        AlertHubEvent.builder()
          .eventCode('alerthub:CapEvent')
          .routingKey('CAPAlert.'+feed_code)
          .body(event)
          .build());
    
CAPEventHandlerService subscribes to these events


  @Subscriber('alerthub:CapEvent')
  public void process(AlertHubEvent ahe) {

And is responsible for matching an alert against subscriptions, posting to elasticsearch and the local archive and any other work



ES setup

http://localhost:9200/_cat/indices


For K8S on linux remember to add these to sysctl.conf

vm.max_map_count=262144
fs.file-max=65536
fs.inotify.max_user_instances = 32768
fs.inotify.max_user_watches = 128000
